"use client";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
// import { selectAuth } from "../../../redux/authslice";
import LoadingScreen from "../user/Components/Loader"
import { useRouter } from "next/navigation";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEye, faEyeSlash } from "@fortawesome/free-solid-svg-icons";
import { signupUser } from "../redux/authActions";

const register = () => {
  const dispatch = useDispatch();
  const router = useRouter();
  const { isLoading, error } = useSelector((state) => state.slice);

  const [userData, setUserData] = useState({
    firstname: "",
    lastname: "",
    email: "",
    password: "",
  });

  const [errors, setErrors] = useState({});
  const [showPassword, setShowPassword] = useState(false);

  const togglePasswordVisibility = () => {
    setShowPassword(!showPassword);
  };

  const handleInputChange = (e) => {
    setUserData({
      ...userData,
      [e.target.name]: e.target.value,
    });
  };

  const validateform = () => {
    const errors = {};

    if (userData.firstname == "" && userData.firstname.length == 0) {
      errors.firstname = "Required*";
    }
    if (userData.lastname == "" && userData.lastname.length == 0) {
      errors.lastname = "Required*";
    }
    if (userData.email == "" && userData.email.length == 0) {
      errors.email = "Required*";
    } else if (!userData.email.match(/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/)) {
      errors.email = "Email not in correct format";
    }
    if (userData.password == "" && userData.password.length == 0) {
      errors.password = "Required*";
    }
    setErrors(errors);

    if (Object.keys(errors).length == 0) {
      return false;
    } else {
      return true;
    }
  };

  const handleSignup = (e) => {
    e.preventDefault();
    let data = validateform();
    if (!data) {
      dispatch(signupUser({ userData, router }));
    }
  };

  return (
    <div>
    {isLoading&& <LoadingScreen/>}
      <div className="register">
        <div className="registerBox">
          <h1>Register</h1>
          <form>
            <label htmlFor="firstname">First Name</label>
            <input type="text" name="firstname" value={userData.firstname} onChange={handleInputChange} placeholder="Enter your FirstName" />
            {<p style={{ color: "red" }}>{errors.firstname}</p>}

            <label htmlFor="lastname">Last Name</label>
            <input type="text" name="lastname" value={userData.lastname} onChange={handleInputChange} placeholder="Enter your LastName" />
            {<p style={{ color: "red" }}>{errors.lastname}</p>}

            <label htmlFor="email">Email</label>
            <input type="email" name="email" value={userData.email} onChange={handleInputChange} placeholder="Enter your Email" />
            {<p style={{ color: "red" }}>{errors.email}</p>}

            <div className="pass-div">
            <label for="password">Password</label>
            <input type={showPassword ? "text" : "password"} name="password" value={userData.password} onChange={handleInputChange} placeholder="Enter your Password" />
            <div className="pass-toggle">
              <FontAwesomeIcon icon={showPassword ? faEyeSlash : faEye} className="password-toggle-icon" onClick={togglePasswordVisibility} />
            </div>
            </div>
           
           
            {<p style={{ color: "red" }}>{errors.password}</p>}
            <button className="regis" type="submit" disabled={isLoading} onClick={handleSignup}>
              {isLoading ? "Signing up..." : "Sign Up"}
            </button>
            {error && <p style={{ color: "red" }}>{error}</p>}
            <br />
            <div style={{ display: "flex" }}>
              <p>Already have an user ?&nbsp;</p>
              <a href="/login" className="text-color">
                <span>Login Here</span>{" "}
              </a>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default register;
