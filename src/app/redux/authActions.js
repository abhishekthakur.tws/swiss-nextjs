import axios from "axios";
import { message } from "antd";
import { BASE_URL } from "../common/common";
import {
  loginStart,
  loginSuccess,
  loginFailure,
  signupStart,
  signupSuccess,
  signupFailure,
  userDetailsStart,
  userDetailsSuccess,
  userDetailsFailure,
  studentVisaInfoStart,
  studentVisaInfoSuccess,
  studentVisaInfoFailure,
  workVisaStart,
  workVisaSuccess,
  workVisaFailure,
  touristVisaInfoStart,
  touristVisaInfoSuccess,
  touristVisaInfoFailure,
  ServicesListingStart,
  ServicesListingSuccess,
  ServicesListingFailure,
  employeeDetailsStart,
  employeeDetailsSuccess,
  employeeDetailsFailure,
  employeeLoginStart,
  employeeLoginSuccess,
  employeeLoginFailure,
  userInfoStart,
  userInfoSuccess,
  userInfoFailure,
  userAssingToEmployeeStart,
  userAssingToEmployeeSuccess,
  userAssingToEmployeeFailure,
  employeeListStart,
  employeeListSuccess,
  employeeListFailure,
  StudentVisaStart,
  StudentVisaSuccess,
  StudentVisaFailure,
  TouristVisaStart,
  TouristVisaSuccess,
  TouristVisaFailure,
  WorkVisaInfoStart,
  WorkVisaInfoSuccess,
  WorkVisaInfoFailure,
  addNriDetailsStart,
  addNriDetailsSuccess,
  addNriDetailsFailure,
  AllNriServicesStart, 
  AllNriServicesSuccess, 
  AllNriServicesFailure,
  GetAllQueriesListStart,
  GetAllQueriesListSuccess,
  GetAllQueriesListFailure,
  GetAssignedUserStart,
  GetAssignedUserSuccess,
  GetAssignedUserFailure,
} from "./authslice";

export const loginUser =
  ({ credentials, router }) =>
    async (dispatch) => {
      const Reqoptions = {
        headers: {
          "Content-Type": "application/json",
        },
      };
      try {
        dispatch(loginStart());
        const response = await axios.post(BASE_URL + "/api/auth/login", credentials, Reqoptions);
        if (response?.data?.status === true) {
          dispatch(loginSuccess({ user: response.data.user, token: response.data.token }));
          localStorage.setItem("accessToken", response?.data?.body?.token?.accessToken);
          localStorage.setItem("role", response?.data?.body?.role);

          // dispatch(
          //   loginSuccess({ user: response.data.user, token: response.data.token })
          // );
          // localStorage.setItem(
          //   "accessToken",
          //   response?.data?.body?.token?.accessToken
          // );

          if (response?.data?.body?.role === "USER") {
            router.push("/");
          } else if (response?.data?.body?.role === "ADMIN") {
            router.push("/admin/pages/dashboard");
          } else if (response?.data?.body?.role === "EMPLOYEE") {
            router.push("/employee/pages/dashboard");
          }
          message.success(response.data.message);
          return response.data;
        } else {
          message.error(response?.data?.message);
        }
      } catch (error) {
        dispatch(loginFailure(error.message));
      }
    };

export const signupUser =
  ({ userData, router }) =>
    async (dispatch) => {
      try {
        dispatch(signupStart());
        const response = await axios.post(BASE_URL + "/api/auth/signUp", userData);
        if (response?.data?.status === true) {
          dispatch(
            signupSuccess({
              user: response.data.user,
              token: response.data.token,
            })
          );
          router.push("/login");
          message.success(response?.data?.message);
          return response?.data;
        } else {
          message.error(response?.data?.message);
        }
      } catch (error) {
        dispatch(signupFailure(error?.message));
      }
    };

export const userDetails = (info) => async (dispatch) => {
  try {
    dispatch(userDetailsStart());
    const response = await axios.post(BASE_URL + "/api/user/userinfo", info);
    if (response?.data?.status === true) {
      dispatch(
        userDetailsSuccess({
          user: response.data.user,
          token: response.data.token,
        })
      );
      message.success(response?.data?.message);
      return response?.data;
    } else {
      message.error(response?.data?.message);
    }
  } catch (error) {
    dispatch(userDetailsFailure(error?.message));
  }
};

export const studentVisaInfo = (formdetail) => async (dispatch) => {
  let formData = new FormData();
  formData.append("fullName", formdetail.fullName);
  formData.append("fatherName", formdetail.fatherName);
  formData.append("dateOfBirth", formdetail.dateOfBirth);
  formData.append("gender", formdetail.gender);
  formData.append("nationality", formdetail.nationality);
  formData.append("maritalStatus", formdetail.maritalStatus);
  formData.append("address", formdetail.address);
  formData.append("email", formdetail.email);
  formData.append("phoneNumber", formdetail.phoneNumber);
  formData.append("academicQualifications", formdetail.academicQualifications);
  formData.append("Institution", formdetail.Institution);
  formData.append("Degree", formdetail.Degree);
  formData.append("sessionDate", formdetail.sessionDate);
  formData.append("currentCountry", formdetail.currentCountry);
  formData.append("destinationCountry", formdetail.destinationCountry);
  formData.append("arrivalDate", formdetail.arrivalDate);
  formData.append("passportNumber", formdetail.passportNumber);
  formData.append("englishProficiencyTestScores", formdetail.englishProficiencyTestScores);
  formData.append("bankStatements", formdetail.bankStatements);
  formData.append("healthInsuranceDetails", formdetail.healthInsuranceDetails);
  formData.append("passportSizedPhotographs", formdetail.passportSizedPhotographs);
  formData.append("scannedDocuments", formdetail.scannedDocuments);
  const requestOptions = {
    headers: {
      platform: "web",
      // authorization: `Bearer ${token}`,
      "Content-Type": "multipart/form-data",
    },
  };
  try {
    dispatch(studentVisaInfoStart());
    const response = await axios.post(BASE_URL + "/api/student/studentVisaInfo", formData,requestOptions);
    if (response?.data?.status === true) {
      dispatch(
        studentVisaInfoSuccess({
          // user: response.data.user,
          // token: response.data.token,
        })
      );
      message.success(response?.data?.message);
      return response?.data;
    } else {
      message.error(response?.data?.message);
    }
  } catch (error) {
    dispatch(studentVisaInfoFailure(error?.message));
  }
};

export const workPermitInfo = (workPermitFields) => async (dispatch) => {
  const token = localStorage.getItem("accessToken");
  let formData = new FormData();
  formData.append("fullName", workPermitFields.fullName);
  formData.append("fatherName", workPermitFields.fatherName);
  formData.append("dateOfBirth", workPermitFields.dateOfBirth);
  formData.append("gender", workPermitFields.gender);
  formData.append("nationality", workPermitFields.nationality);
  formData.append("maritalStatus", workPermitFields.maritalStatus);
  formData.append("address", workPermitFields.address);
  formData.append("email", workPermitFields.email);
  formData.append("phoneNumber", workPermitFields.phoneNumber);
  formData.append("academicQualifications", workPermitFields.academicQualifications);
  formData.append("degree", workPermitFields.degree);
  formData.append("passportNumber", workPermitFields.passportNumber);
  formData.append("jobTitle", workPermitFields.jobTitle);
  formData.append("jobDescription", workPermitFields.jobDescription);
  formData.append("employerName", workPermitFields.employerName);
  formData.append("durationOfEmployment", workPermitFields.durationOfEmployment);
  formData.append("salaryAndOtherCompensationDetails", workPermitFields.salaryAndOtherCompensationDetails);
  formData.append("criminalBackground", workPermitFields.criminalBackground);
  formData.append("supportingDocuments", workPermitFields.supportingDocuments);
  formData.append("englishProficiencyTestScores", workPermitFields.englishProficiencyTestScores);
  formData.append("bankStatements", workPermitFields.bankStatements);
  formData.append("healthInsuranceDetails", workPermitFields.healthInsuranceDetails);
  formData.append("passportSizedPhotographs", workPermitFields.passportSizedPhotographs);
  formData.append("scannedDocuments", workPermitFields.scannedDocuments);
  formData.append("professionalCertifications", workPermitFields.professionalCertifications);
  const reqOptions = {
    headers: {
      platform: "web",
      authorization: `Bearer ${token}`,
      "Content-Type": "multipart/form-data",
    },
  };

  try {
    dispatch(workVisaStart());
    const response = await axios.post(BASE_URL + "/api/work/workVisaInfo", formData, reqOptions);
    if (response?.data?.status === true) {
      dispatch(
        workVisaSuccess({
          user: response.data.user,
          token: response.data.token,
        })
      );
      message.success(response?.data?.message);
      return response?.data;
    } else {
      message.error(response?.data?.message);
    }
  } catch (error) {
    dispatch(workVisaFailure(error?.message));
  }
};

export const touristVisaInfo = (userData) => async (dispatch) => {
  let formData = new FormData();
  formData.append("fullName", userData.fullName);
  formData.append("fatherName", userData.fatherName);
  formData.append("dateOfBirth", userData.dateOfBirth);
  formData.append("gender", userData.gender);
  formData.append("nationality", userData.nationality);
  formData.append("maritalStatus", userData.maritalStatus);
  formData.append("address", userData.address);
  formData.append("email", userData.email);
  formData.append("phoneNumber", userData.phoneNumber);
  formData.append("academicQualifications", userData.academicQualifications);
  formData.append("Institution", userData.Institution);
  formData.append("Degree", userData.Degree);
  formData.append("sessionDate", userData.sessionDate);
  formData.append("currentCountry", userData.currentCountry);
  formData.append("destinationCountry", userData.destinationCountry);
  formData.append("arrivalDate", userData.arrivalDate);
  formData.append("passportNumber", userData.passportNumber);
  formData.append("passportIssueDate", userData.passportIssueDate);
  formData.append("passportExpiryDate", userData.passportExpiryDate);
  formData.append("englishProficiencyTestScores", userData.englishProficiencyTestScores);
  formData.append("bankStatements", userData.bankStatements);
  formData.append("healthInsuranceDetails", userData.healthInsuranceDetails);
  formData.append("passportSizedPhotographs", userData.passportSizedPhotographs);
  formData.append("scannedDocuments", userData.scannedDocuments);
  formData.append("purposeOfVisit", userData.purposeOfVisit);

  // const Reqoptions = {
  //   headers: {
  //     "Content-Type": "multipart/form-data",
  //   },
  // };
  const reqOptions = {
    headers: {
      platform: "web",
      // authorization: `Bearer ${token}`,
      "Content-Type": "multipart/form-data",
    },
    origin,
  };

  try {
    dispatch(touristVisaInfoStart());
    const response = await axios.post(BASE_URL + "/api/tourist/touristVisaInfo", formData, reqOptions);
    if (response?.data?.status === true) {
      dispatch(
        touristVisaInfoSuccess({
          user: response.data.user,
          token: response.data.token,
        })
      );
      message.success(response?.data?.message);
      return response?.data;
    } else {
      message.error(response?.data?.message);
    }
  } catch (error) {
    dispatch(touristVisaInfoFailure(error?.message));
    // message.error(error?.message);
  }
};

export const ServicesListing = () => async (dispatch) => {
  try {
    dispatch(ServicesListingStart());
    const response = await axios.get(BASE_URL + `/api/nri/ServicesListing`);
    if (response?.data?.status === true) {
      dispatch(ServicesListingSuccess(response?.data?.body));
      message.success(response?.data?.message);
      // return response?.data?.body;
    } else {
      message.error(response?.data?.message);
    }
  } catch (error) {
    dispatch(ServicesListingFailure(error?.message));
  }
};

export const userInfo = () => async (dispatch) => {
  try {
    dispatch(userInfoStart());
    const response = await axios.get(BASE_URL + "/api/user/getuserinfo");
    if (response?.data?.status === true) {
      dispatch(userInfoSuccess(response?.data?.body));
      // message.success(response?.data?.message);
      return response?.data?.body;
    } else {
      message.error(response?.data?.message);
    }
  } catch (error) {
    dispatch(userInfoFailure(error?.message));
  }
};

export const employeeDetailsInfo = (employeeDetails) => async (dispatch) => {
  const reqOptions = {
    headers: {
      "Content-Type": "application/json",
    },
  };
  try {
    dispatch(employeeDetailsStart());
    const response = await axios.post(BASE_URL + "/api/employee/addEmployee", employeeDetails, reqOptions);
    
    if (response?.data?.status === true) {
      dispatch(
        employeeDetailsSuccess({
          user: response.data.user,
          token: response.data.token,
        })
      );
    } else {
      throw new Error(response?.data?.message);
    }
  } catch (error) {
    dispatch(employeeDetailsFailure(error?.message));
    // message.error(error?.message);
  }
};

export const employeeLogin = ({ credentials, router }) => async (dispatch) => {
  const reqOptions = {
    headers: {
      "Content-Type": "application/json",
    },
  };
  try {
    dispatch(employeeLoginStart());
    const response = await axios.post(BASE_URL + "/api/employee/loginEmployee", credentials, reqOptions);
    console.log("responseresponseresponseresponse",response);
    if (response?.data?.status) {
      dispatch(employeeLoginSuccess({ user: response.data.user, token: response.data.token }));
          localStorage.setItem("accessToken", response?.data?.body?.token?.accessToken);
          localStorage.setItem("role", response?.data?.body?.role);
      router.push("employee/pages/dashboard");
      message.success(response.data.message);
      return response.data;
    } else {
      message.error(response?.data?.message);
    }
  } catch (error) {
    dispatch(employeeLoginFailure(error?.message));
    // message.error(error?.message);
  }
};

export const addNriDetails = (nridata) => async (dispatch) => {
  const reqOptions = {
    headers: {
      "Content-Type": "application/json",
    },
  };
  try {
    dispatch(addNriDetailsStart());
    const response = await axios.post(BASE_URL + "/api/nri/addNriDetails", nridata, reqOptions);

    if (response?.data?.status === true) {
      dispatch(
        addNriDetailsSuccess({
          user: response.data.user,
          token: response.data.token,
        })
      );
      message.success(response.data.message);
      return response.data;
    } else {
      message.error(response?.data?.message);
    }
  } catch (error) {
    dispatch(addNriDetailsFailure(error?.message));
  }
};

export const userAssingToEmployee = (empData) => async (dispatch) => {
 
  try {
    dispatch(userAssingToEmployeeStart());
    const response = await axios.post(BASE_URL + "/api/userinfo/userAssingToEmployee",empData);
    if (response?.data?.status === true) {
      dispatch(userAssingToEmployeeSuccess(response?.data?.body));
      return response?.data?.body;
    } else { 
      message.success(response?.data?.message);

      // message.error(response?.data?.message);
    }
    dispatch(userInfo());
    dispatch(AllNriServices());
    dispatch(TouristVisaListing());
    dispatch(WorkVisaListing())
    dispatch(StudentVisaListing())                

  } catch (error) {
    dispatch(userAssingToEmployeeFailure(error?.message));
  }
};

export const employeeList = () => async (dispatch) => {
  try {
    dispatch(employeeListStart());
    const response = await axios.get(BASE_URL + "/api/employee/employeeListing");
    console.log("responseemployeeList", response);
    if (response?.data?.status === true) {
      dispatch(employeeListSuccess(response?.data?.data));
    }
  }
  catch (error) {

    dispatch(employeeListFailure(error?.message));
  }
};

export const StudentVisaListing = () => async (dispatch) => {
  try {
    dispatch(StudentVisaStart());
    const response = await axios.get(BASE_URL + `/api/student/getstudentVisaInfo`);
    if (response?.data?.status === true) {
      dispatch(StudentVisaSuccess(response?.data?.body));
      // message.success(response?.data?.message);
      return response?.data?.body;
    } else {
      message.error(response?.data?.message);
    }
  }
  catch {
    dispatch(StudentVisaFailure(error?.message));
  }
};

export const TouristVisaListing = () => async (dispatch) => {
  try {
    dispatch(TouristVisaStart());
    const response = await axios.get(BASE_URL + "/api/tourist/gettouristVisaInfo");
    if (response?.data?.status === true) {
      dispatch(TouristVisaSuccess(response?.data?.body));
      // message.success(response?.data?.message);
      return response?.data?.body;
    } else {
      // message.error(response?.data?.message);
    }
  } catch (error) {
    dispatch(TouristVisaFailure(error?.message));
  }
};

export const WorkVisaListing = () => async (dispatch) => {
  try {
    dispatch(WorkVisaInfoStart());
    const response = await axios.get(BASE_URL + `/api/work/getWorkVisaInfo`);
    if (response?.data?.status === true) {
      dispatch(WorkVisaInfoSuccess(response?.data?.body));
      // message.success(response?.data?.message);
      return response?.data?.body;
    } else {
      message.error(response?.data?.message);
    }
  } catch (error) {
    dispatch(WorkVisaInfoFailure(error?.message));
  }
};

export const AllNriServices = () => async (dispatch) => {
  try {
    dispatch(AllNriServicesStart());
    const response = await axios.get(BASE_URL + '/api/nri/getAllNriServices');  // Fix the syntax error here
    if (response?.data?.status === true) {
      dispatch(AllNriServicesSuccess(response?.data?.body));
      // message.success(response?.data?.message);
      return response?.data?.body;
    } else {
      message.error(response?.data?.message);
    }
  } catch (error) {
    dispatch(AllNriServicesFailure(error?.message));
  }
};

export const AllQueries = () => async (dispatch) => {
  try {
    dispatch(GetAllQueriesListStart());
    const response = await axios.get(BASE_URL + '/api/employee/employeeListing');
    if (response?.data?.status === true) {
      dispatch(GetAllQueriesListSuccess(response?.data?.data));
      message.success(response?.data?.message);
      return response?.data?.data;
    } else {
      message.error(response?.data?.message);
    }
  } catch (error) {
    dispatch(GetAllQueriesListFailure(error?.message));
  }
};

export const GetAllAssignedEmployeeData = () => async (dispatch) => {
  try {
    dispatch(GetAssignedUserStart());
    const response = await axios.get(BASE_URL + '/api/userinfo/getEmployeesStatus');
    if (response?.data?.status === true) {
      dispatch(GetAssignedUserSuccess(response?.data?.body));
      message.success(response?.data?.message);
      return response?.data?.body;
    } else {
      message.error(response?.data?.message);
    }
  } catch (error) {
    dispatch(GetAssignedUserFailure(error?.message));
  }
};

