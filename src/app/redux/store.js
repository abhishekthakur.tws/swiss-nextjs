import { configureStore } from "@reduxjs/toolkit";
import counterSlice from "../../../src/app/redux/authslice";
const store = configureStore({
  reducer: {
    slice: counterSlice,
  },
});

const { dispatch } = store;
export { store, dispatch };
