import { createSlice } from "@reduxjs/toolkit";

const authSlice = createSlice({
  name: "authSlice",
  initialState: {
    user: null,
    token: null,
    isLoading: false,
    error: null,
    getUserdata: [],
    ServicesData: [],
    UsersData: [],
    employeeListData:[],
    StudentVisaList: [],
    TouristVisaList: [],
    WorkVisaList: [],
    AllnriData:[],
    GetAllQueriesList:[],
    AssignedUserList:[],
  },

  reducers: {
    loginStart: (state) => {
      state.isLoading = true;
      state.error = null;
    },
    loginSuccess: (state, action) => {
      state.isLoading = false;
      state.user = action.payload.user;
      state.token = action.payload.token;
    },
    loginFailure: (state, action) => {
      state.isLoading = false;
      state.error = action.payload;
    },
    signupStart: (state) => {
      state.isLoading = true;
      state.error = null;
    },
    signupSuccess: (state, action) => {
      state.isLoading = false;
      state.user = action.payload.user;
      state.token = action.payload.token;
    },
    signupFailure: (state, action) => {
      state.isLoading = false;
      state.error = action.payload;
    },
    userDetailsStart: (state, action) => {
      state.isLoading = true;
      state.error = null;
    },
    userDetailsSuccess: (state, action) => {
      state.isLoading = false;
      state.userDetailsData = action.payload;
    },
    userDetailsFailure: (state, action) => {
      state.isLoading = false;
      state.error = action.payload;
    },
    studentVisaInfoStart: (state, action) => {
      state.isLoading = true;
      state.error = null;
    },
    studentVisaInfoSuccess: (state, action) => {
      state.isLoading = false;
      state.error = action.payload;
    },
    studentVisaInfoFailure: (state, action) => {
      state.isLoading = false;
      state.error = action.payload;
    },
    workVisaStart: (state, action) => {
      state.isLoading = true;
      state.error = null;
    },
    workVisaSuccess: (state, action) => {
      state.isLoading = false;
      state.error = action.payload;
    },
    workVisaFailure: (state, action) => {
      state.isLoading = false;
      state.error = action.payload;
    },
    touristVisaInfoStart: (state, action) => {
      state.isLoading = true;
      state.error = null;
    },
    touristVisaInfoSuccess: (state, action) => {
      state.isLoading = false;
      state.error = action.payload;
    },
    touristVisaInfoFailure: (state, action) => {
      state.isLoading = false;
      state.error = action.payload;
    },
    ServicesListingStart: (state) => {
      state.isLoading = true;
      state.error = null;
    },
    ServicesListingSuccess: (state, action) => {
      state.isLoading = false;
      state.ServicesData = action.payload;
    },
    ServicesListingFailure: (state, action) => {
      state.isLoading = false;
      state.error = action.payload;
    }
    ,
    employeeLoginStart: (state) => {
      state.isLoading = true;
      state.error = null;
    },
    employeeLoginSuccess: (state, action) => {
      state.isLoading = false;
      state.user = action.payload.user;
      state.token = action.payload.token;
    },
    employeeLoginFailure: (state, action) => {
      state.isLoading = false;
      state.error = action.payload;
    },
    employeeDetailsStart: (state, action) => {
      state.isLoading = true;
      state.error = null;
    },
    employeeDetailsSuccess: (state, action) => {
      state.isLoading = false;
      state.ServicesData = action.payload;
    },
    employeeDetailsFailure: (state, action) => {
      state.isLoading = false;
      state.error = action.payload;
    },
    userInfoStart: (state, action) => {
      state.isLoading = true;
      state.error = null;
    },
    userInfoSuccess: (state, action) => {
      state.isLoading = false;
      state.UsersData = action.payload;
    },
    userInfoFailure: (state, action) => {
      state.isLoading = false;
      state.error = action.payload;
    },
    userAssingToEmployeeStart: (state, action) => {
      state.isLoading = true;
      state.error = null;
    },
    userAssingToEmployeeSuccess: (state, action) => {
      state.isLoading = false;
      state.error = action.payload;
    },
    userAssingToEmployeeFailure: (state, action) => {
      state.isLoading = false;
      state.error = action.payload;
    },
    employeeListStart: (state, action) => {
      state.isLoading = true;
      state.error = null;
    },
    employeeListSuccess: (state, action) => {
      state.isLoading = false;
      state.employeeListData = action.payload;
    },
    employeeListFailure: (state, action) => {
      state.isLoading = false;
      state.error = action.payload;
    },

    StudentVisaStart: (state, action) => {
      state.isLoading = true;
      state.error = null;
    },
    StudentVisaSuccess: (state, action) => {
      state.isLoading = false;
      state.StudentVisaList = action.payload;
    },
    StudentVisaFailure: (state, action) => {
      state.isLoading = false;
      state.error = action.payload;
    },
    TouristVisaStart: (state, action) => {
      state.isLoading = true;
      state.error = null;
    },
    TouristVisaSuccess: (state, action) => {
      state.isLoading = false;
      state.TouristVisaList = action.payload;
    },
    TouristVisaFailure: (state, action) => {
      state.isLoading = false;
      state.error = action.payload;
    },
    WorkVisaInfoStart: (state, action) => {
      state.isLoading = true;
      state.error = null;
    },
    WorkVisaInfoSuccess: (state, action) => {
      state.isLoading = false;
      state.WorkVisaList = action.payload;
    },
    WorkVisaInfoFailure: (state, action) => {
      state.isLoading = false;
      state.error = action.payload;
    },
    addNriDetailsStart: (state, action) => {
      state.isLoading = true;
      state.error = null;
    },
    addNriDetailsSuccess: (state, action) => {
      state.isLoading = false;
      state.error = action.payload;
    },
    addNriDetailsFailure: (state, action) => {
      state.isLoading = false;
      state.error = action.payload;
    },
     AllNriServicesStart: (state, action) => {
      state.isLoading = true;
      state.error = null;
    },
    AllNriServicesSuccess: (state, action) => {
      state.isLoading = false;
      state.AllnriData = action.payload;
    },
    AllNriServicesFailure: (state, action) => {
      state.isLoading = false;
      state.error = action.payload;
    },
    GetAllQueriesListStart: (state, action) => {
      state.isLoading = true;
      state.error = null;
    },
    GetAllQueriesListSuccess: (state, action) => {
      state.isLoading = false;
      state.GetAllQueriesList = action.payload;
    },
    GetAllQueriesListFailure: (state, action) => {
      state.isLoading = false;
      state.error = action.payload;
    },
    GetAssignedUserStart: (state, action) => {
      state.isLoading = true;
      state.error = null;
    },
    GetAssignedUserSuccess: (state, action) => {
      state.isLoading = false;
      state.AssignedUserList = action.payload;
    },
    GetAssignedUserFailure: (state, action) => {
      state.isLoading = false;
      state.error = action.payload;
    },
  },
});

export const {
  loginStart,
  loginSuccess,
  loginFailure,
  signupStart,
  signupSuccess,
  signupFailure,
  userDetailsStart,
  userDetailsSuccess,
  userDetailsFailure,
  studentVisaInfoStart,
  studentVisaInfoSuccess,
  studentVisaInfoFailure,
  workVisaStart,
  workVisaSuccess,
  workVisaFailure,
  touristVisaInfoStart,
  touristVisaInfoSuccess,
  touristVisaInfoFailure,
  ServicesListingStart,
  ServicesListingSuccess,
  ServicesListingFailure,
  employeeDetailsStart,
  employeeDetailsSuccess,
  employeeDetailsFailure,
  employeeLoginStart,
  employeeLoginSuccess,
  employeeLoginFailure,
  userInfoStart,
  userInfoSuccess,
  userInfoFailure,
  employeeListStart,
  employeeListSuccess,
  employeeListFailure,
  userAssingToEmployeeStart,
  userAssingToEmployeeSuccess,
  userAssingToEmployeeFailure,
  StudentVisaStart,
  StudentVisaSuccess,
  StudentVisaFailure,
  TouristVisaStart,
  TouristVisaSuccess,
  TouristVisaFailure,
  WorkVisaInfoStart,
  WorkVisaInfoSuccess,
  WorkVisaInfoFailure,
  addNriDetailsStart,
  addNriDetailsSuccess,
  addNriDetailsFailure,
  AllNriServicesStart, 
  AllNriServicesSuccess, 
  AllNriServicesFailure,
  GetAllQueriesListStart,
  GetAllQueriesListSuccess,
  GetAllQueriesListFailure,
  GetAssignedUserStart,
  GetAssignedUserSuccess,
  GetAssignedUserFailure,
} = authSlice.actions;

export default authSlice.reducer;
