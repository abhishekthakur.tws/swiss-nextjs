"use client";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { loginUser } from "../redux/authActions";
import { useRouter } from "next/navigation";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEye, faEyeSlash } from "@fortawesome/free-solid-svg-icons";
import LoadingScreen from "../user/Components/Loader";

const login = () => {
  const [credentials, setCredentials] = useState({
    email: "",
    password: "",
  });
  const [errors, setErrors] = useState("");
  const [showPassword, setShowPassword] = useState(false);
  const [loading, setLoading] = useState(true);

  const router = useRouter();
  const dispatch = useDispatch();
  const { isLoading, error } = useSelector((state) => state.slice);
  const handleInputChange = (e) => {
    setCredentials({
      ...credentials,
      [e.target.name]: e.target.value,
    });
  };

  const togglePasswordVisibility = () => {
    setShowPassword(!showPassword);
  };

  const validateform = () => {
    const errors = {};

    if (credentials.email == "" && credentials.email.length == 0) {
      errors.email = "Required*";
    } else if (!credentials.email.match(/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/)) {
      errors.email = "Email not in correct format";
    }
    if (credentials.password == "" && credentials.password.length == 0) {
      errors.password = "Required*";
    }
    setErrors(errors);

    if (Object.keys(errors).length == 0) {
      return false;
    } else {
      return true;
    }
  };

  const handleLogin = (e) => {
    e.preventDefault();
    let data = validateform();
    if (!data) {
      dispatch(loginUser({ credentials, router }));
    }
    setLoading(false)
  };

  return (
    <>
      {isLoading && <LoadingScreen />}
      <div className="login">
        <div className="login-container">
          <h2>Login</h2>
          <form>
            <br />
            <form action="#" method="post">
              <label for="email">Email</label>
              <input type="email" name="email" value={credentials.email} onChange={handleInputChange} placeholder="Enter your Email" />
              {<p style={{ color: "red" }}>{errors.email}</p>}
              <br />
              <div className="pass-div">
                <label for="password">Password</label>
                <input type={showPassword ? "text" : "password"} name="password" value={credentials.password} onChange={handleInputChange} placeholder="Enter your Password" />
                <div className="pass-toggle">
                  <FontAwesomeIcon icon={showPassword ? faEyeSlash : faEye} className="password-toggle-icon" onClick={togglePasswordVisibility} />
                </div>
              </div>

              {<p style={{ color: "red" }}>{errors.password}</p>}
              <button className="axc" type="submit" disabled={isLoading} onClick={handleLogin}>
                {isLoading ? "Logging in..." : "Login"}
              </button>
              {error && <p style={{ color: "red" }}>{error}</p>}
              <div style={{ display: "flex" }}>
                <p>Don't have an account ?&nbsp;</p>
                <a href="/register" className="text-color">
                  <span>Register Here</span>{" "}
                </a>
              </div>
            </form>
          </form>
        </div>
      </div>
    </>
  );
};

export default login;
