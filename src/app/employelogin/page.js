"use client"
import React, { useState } from 'react';
import "./employelogin.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEye, faEyeSlash } from "@fortawesome/free-solid-svg-icons";
import { useDispatch, useSelector } from 'react-redux';
import { employeeLogin } from '../redux/authActions';
import LoadingScreen from "../user/Components/Loader";
import { useRouter } from "next/navigation";

const page = () => {
    const [errors, setErrors] = useState("");
    const [showPassword, setShowPassword] = useState(false);
    const { isLoading, error } = useSelector((state) => state.slice);
    const [credentials, setCredentials] = useState({
        employeeCode: "",
        password: "",
      });

      const dispatch = useDispatch()
      const router = useRouter();

      const validateform = () => {
        const errors = {};
        if (!credentials.employeeCode || credentials.employeeCode.trim() === "") {
          errors.employeeCode = "Required*";
        }
        if (!credentials.password || credentials.password.trim() === "") {
          errors.password = "Required*";
        }
        setErrors(errors);
        return Object.keys(errors).length > 0;
      };

      const togglePasswordVisibility = () => {
        setShowPassword(!showPassword);
      };

      const handleInputChange = (e) => {
        setCredentials({
          ...credentials,
          [e.target.name]: e.target.value,
        });
      };

  const handleLogin = (e) => {
    e.preventDefault();
    let data = validateform();
    if (!data) {
      dispatch(employeeLogin({credentials,router}));
    }
  };
    
    return (
        <div>
        {isLoading && <LoadingScreen />}
        <div className="login">
        <div className="login-container">
          <h2> Employe Login</h2>
          <form>
            <br />
            <form action="#" method="post">
              <label for="Employe Code">Employe Code</label>
              <input type="Employe Code" name="employeeCode" value={credentials.employeeCode} onChange={handleInputChange}   placeholder="Enter your Code" />
              {<p style={{ color: "red" }}>{errors.employeeCode}</p>}
              <br />
              <div className="pass-div">
                <label for="password">Password</label>
                <input type={showPassword ? "text" : "password"} name="password" value={credentials.password} onChange={handleInputChange} placeholder="Enter your Password" />
                <div className="pass-toggle">
                  <FontAwesomeIcon icon={showPassword ? faEyeSlash : faEye} className="password-toggle-icon" onClick={togglePasswordVisibility} />
                </div>
              </div>

              {<p style={{ color: "red" }}>{errors.password}</p>}
              <button className="axc" type="submit" disabled={isLoading}  onClick={handleLogin}>
                {isLoading ? "Logging in..." : "Login"}
              </button>
              {error && <p style={{ color: "red" }}>{error}</p>}
             
            </form>
          </form>
        </div>
      </div>
        </div>
    )
}

export default page