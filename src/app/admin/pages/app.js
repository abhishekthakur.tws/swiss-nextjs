import { Provider } from "react-redux";
import store from "../../store";
import Home from "../../user/Components";

function MyApp({ Home }) {
  return (
    <Provider store={store}>
      <Home />
    </Provider>
  );
}

export default MyApp;
