"use client";
import React, { useEffect, useState } from "react";
import Navbar from "@/app/user/Components/Navbar";
import Footer from "@/app/user/Components/Footer";
import { Space, Table, Tag, Button, Modal, Typography, Dropdown, Menu, Form, Input, Checkbox } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { employeeDetailsInfo, getUserDetails, userDetails, userInfo, userAssingToEmployee, employeeList } from "../../../redux/authActions";
import Sidebar from "../../adminComponents/sidebar";
import "../../../../app/globals.css";
import { BASE_URL } from "@/app/common/common";
import axios from 'axios';
import Loaderscreen from "../../../user/Components/Loader"

const page = () => {
  const [form] = Form.useForm();
  const dispatch = useDispatch();
  const [selectedEmployeeCode, setSelectedEmployeeCode] = useState(null);
  const { UsersData,employeeListData,isLoading } = useSelector((state) => state.slice);

  const [empData, setEmpData] = useState({
    user_assign_id: [],
    employeeCode: "",
  });

  const [employeeDetails, setEmployeeDetails] = useState({
    name: "",
    email: "",
    password: "",
    phone_no: "",
    employeeCode: ""
  });

  const [isModalOpen, setIsModalOpen] = useState(false);

  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleOk = () => {
    dispatch(employeeDetailsInfo(employeeDetails));
    setIsModalOpen(false);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  const columns = [
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
      render: (text) => <a>{text}</a>,
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
    },
    {
      title: "Contact",
      dataIndex: "contactNo",
      key: "contactNo",
    },
    {
      title: "Destination",
      dataIndex: "Destination",
      key: "Destination",
    },
    {
      title: "Select",
      key: "select",
      render: (_, record) => (
        <Space size="middle">
          <Checkbox onChange={(e) => handleCheckboxChange(record.id, e.target.checked)} />
        </Space>
      ),
    },
  ];

  const data = [
    {
      key: "1",
      name: "John Brown",
      age: 32,
      address: "New York No. 1 Lake Park",
      // tags: ['nice', 'developer'],
    },
    {
      key: "2",
      name: "Jim Green",
      age: 42,
      address: "London No. 1 Lake Park",
      // tags: ['loser'],
    },
    {
      key: "3",
      name: "Joe Black",
      age: 32,
      address: "Sydney No. 1 Lake Park",
      // tags: ['cool', 'teacher'],
    },
  ];

  useEffect(() => {
    return(()=>{
      dispatch(userInfo());
      dispatch(employeeList())
    })
   
  }, [dispatch]);

  const handleChange = (e) => {
    setEmployeeDetails({
      ...employeeDetails,
      [e.target.name]: e.target.value,
    });
  };

  const handleSubmitEmployee = () => {
    dispatch(userAssingToEmployee(empData));
  };

  const handleCheckboxChange = (key, checked) => {
    if (checked) {
      setEmpData((prevData) => ({
        ...prevData,
        user_assign_id: [...prevData.user_assign_id, key],
      }));
    } else {
      setEmpData((prevData) => ({
        ...prevData,
        user_assign_id: prevData.user_assign_id.filter((existingKey) => existingKey !== key),
      }));
    }
  };


  const empId = (selectedValue) => {
    setSelectedEmployeeCode(selectedValue)
    setEmpData((prevData) => ({
      ...prevData,
      employeeCode: selectedValue
    })); 
  };
  
  const menu = (
    <Menu>
      {employeeListData.map((data) => (
        <Menu.Item key={data.id} onClick={() => empId(data.employeeCode)}>
          {data.employeeCode}
        </Menu.Item>
      ))}
    </Menu>
  );
  
  const downloadData = async () => {
    try {
      const response = await axios.get(BASE_URL + "/api/user/getuserinfo", {
        responseType: 'json',
      });
      const jsonData = response.data.body;
      if (!jsonData) {
        console.error('Error: API response does not contain valid data.');
        return;
      }
      const csvContent = convertJsonToCsv(jsonData);
      const blob = new Blob([csvContent], { type: 'text/csv;charset=utf-8;' });
      const link = document.createElement('a');
      const fileName = 'user_data.csv';
      link.href = window.URL.createObjectURL(blob);
      link.download = fileName;
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    } catch (error) {
      console.error('Error creating CSV:', error);
    }
  };
  
  // Function to convert JSON to CSV format
  const convertJsonToCsv = (jsonData) => {
    const header = Object.keys(jsonData[0]).join(',');
    const rows = jsonData.map(obj => Object.values(obj).join(','));
    return `${header}\n${rows.join('\n')}`;
  };

  return (
    <div>
    {isLoading && <Loaderscreen />}
      <Navbar />

      <div className="col-lg-12 side-div">
        <div>
          <Sidebar />
        </div>

        <div className="table-div">
          <div className="bbtn-div">
          <Button
              type="text"
              onClick={downloadData}
              style={{
                marginLeft: '20px',
                backgroundColor: "#294908",
                color: "#fff",
              }}
            >
              Download Data
            </Button>
            <Button
              type="text"
              onClick={showModal}
              style={{
                marginRight: "10px",
                marginBottom:"10px",
                float: "right",
                backgroundColor: "#294908",
                color: "#fff",
              }}
            >
              Add Employee
            </Button>
          </div>
          <div>
            <Table columns={columns} dataSource={UsersData} 
            footer={() => (
              <div style={{ textAlign: 'right' }}>
                <Dropdown overlay={menu}>
                  <Button>
                  {selectedEmployeeCode ? ` ${selectedEmployeeCode}` : 'Select Employee'}
                  </Button>
                </Dropdown>
                &nbsp;
                <Button type="primary" style={{ marginRight: 8 }} onClick={()=>handleSubmitEmployee(empId)}>
                  Send
                </Button>
              </div>
            )}
            />
          </div>
        </div>
      </div>

      <Modal
        title="Add Employee Details"
        visible={isModalOpen}
        // onOk={handleOk}
        onCancel={handleCancel}
        footer={[
          <Button key="cancel" onClick={handleCancel}>
            {" "}
            Cancel{" "}
          </Button>,
          <Button key="submit" type="primary" onClick={handleOk} style={{ backgroundColor: "#294908", color: "#fff" }}>
            {" "}
            Submit{" "}
          </Button>,
        ]}
      >
        <Form form={form}>
          <Form.Item label={<Typography.Title level={5}>Name</Typography.Title>} name="name" rules={[{ required: true, message: "Please enter a name" }]}>
            <Input placeholder="Name" name="name" onChange={handleChange} />
          </Form.Item>
          
          <Form.Item label={<Typography.Title level={5}>Employe Code</Typography.Title>} name="employeeCode" rules={[{ required: true, message: "Please enter a Employe Code" }]}>
          <Input placeholder="Code" name="employeeCode" onChange={handleChange} />
        </Form.Item>

          <Form.Item
            label={<Typography.Title level={5}>Email</Typography.Title>}
            name="email"
            rules={[
              { required: true, message: "Please enter an email" },
              { type: "email", message: "Please enter a valid email address" },
            ]}
          >
            <Input placeholder="Email" name="email" onChange={handleChange} />
          </Form.Item>

          <Form.Item label={<Typography.Title level={5}>Password</Typography.Title>} name="password" rules={[{ required: true, message: "Please enter a password" }]}>
            <Input.Password placeholder="Password" name="password" onChange={handleChange} />
          </Form.Item>

          <Form.Item label={<Typography.Title level={5}>Phonenumber</Typography.Title>} name="phonenumber" rules={[{ required: true, message: "Please enter a phone number" }]}>
            <Input placeholder="Phonenumber" name="phone_no" onChange={handleChange} />
          </Form.Item>
        </Form>
      </Modal>
      <Footer />
    </div>
  );
};

export default page;
