"use client"
import React, { useEffect } from 'react';
import Navbar from '../../../user/Components/Navbar';
import Sidebar from '../../adminComponents/sidebar';
import Footer from '../../../user/Components/Footer';
import { Table } from "antd";
import { GetAllAssignedEmployeeData } from "../../../redux/authActions";
import { useDispatch, useSelector } from "react-redux";
import Loaderscreen from "../../../user/Components/Loader"


const page = () => {
  const dispatch = useDispatch();
  const { AssignedUserList,isLoading} = useSelector((state) => state.slice);

  useEffect(() => {
    return(()=>{
      dispatch(GetAllAssignedEmployeeData());
    })
  },[])
      
  const columns = [
      {
        title: 'Employee Code',
        dataIndex: 'employeeCode',
        key: 'employeeCode',
      },
      {
        title: 'Total Assigned',
        dataIndex: 'totalAssigned',
        key: 'totalAssigned',
      },
      {
        title: 'Pending',
        dataIndex: 'pendingCount',
        key: 'pendingCount',
      },
      {
        title: 'Inprogress',
        dataIndex: 'inprogressCount',
        key: 'Inprogress Count',
      },
      {
        title: 'Completed',
        dataIndex: 'completedCount',
        key: 'completedCount',
      },          
  ];
      
  return (
    <div>
     {isLoading && <Loaderscreen />}
    <Navbar />
    <div className="col-lg-12 side-div">
      <div>
        <Sidebar />
      </div>
      <div className="table-div">
        <div>
        <Table dataSource={AssignedUserList} columns={columns} />
        </div>
      </div>
    </div>
    <Footer />
  </div>
  )
}

export default page;

