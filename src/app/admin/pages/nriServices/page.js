"use client";
import React, { useEffect, useState } from "react";
import Navbar from "@/app/user/Components/Navbar";
import Sidebar from "../../adminComponents/sidebar";
import Footer from "@/app/user/Components/Footer";
import { useDispatch, useSelector } from "react-redux";
import { AllNriServices, employeeList, userAssingToEmployee, userInfo } from "@/app/redux/authActions";
import { Space, Table, Tag, Button, Modal, Typography, Dropdown, Menu, Form, Input, Checkbox } from "antd";
import Loaderscreen from "../../../user/Components/Loader"
import axios from 'axios';
import { BASE_URL } from "@/app/common/common";



const page = () => {
  const dispatch = useDispatch();

  const [selectedEmployeeCodee, setSelectedEmployeeCodee] = useState(null);
  const { AllnriData, employeeListData, isLoading } = useSelector((state) => state.slice);

  const [empData, setEmpData] = useState({
    user_assign_id: [],
    employeeCode: "",
    category: 'nri',
  });

  const columns = [
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
      render: (text) => <a>{text}</a>,
    },
    {
      title: 'Contact',
      dataIndex: 'contact_no',
      key: 'contact_no',
    },
    {
      title: 'Service Id',
      dataIndex: 'service_id',
      key: 'service_id',
    },
    {
      title: 'Select',
      render: (_, record) => (
        <Space size="middle">
          <Checkbox onChange={(e) => handleCheckboxChange(record.id, e.target.checked)} />
        </Space>
      ),
    },
  ];

  useEffect(() => { 
    return () => {
      dispatch(AllNriServices());
    dispatch(employeeList());
    };
  }, []);

  const handleSubmitEmploy = () => {
    dispatch(userAssingToEmployee(empData));
  };

  const handleCheckboxChange = (key, checked) => {
    if (checked) {
      setEmpData((prevData) => ({
        ...prevData,
        user_assign_id: [...prevData.user_assign_id, key],
      }));
    } else {
      setEmpData((prevData) => ({
        ...prevData,
        user_assign_id: prevData.user_assign_id.filter((existingKey) => existingKey !== key),
      }));
    }
  };

  const empId = (selectedValue) => {
    setSelectedEmployeeCodee(selectedValue);
    setEmpData((prevData) => ({
      ...prevData,
      employeeCode: selectedValue,
    }));
  };

  const Item = (
    <Menu>
      {employeeListData.map((data) => (
        <Menu.Item key={data.id} onClick={() => empId(data.employeeCode)}>
          {data.employeeCode}
        </Menu.Item>
      ))}
    </Menu>
  );



  const downloadData = async () => {
    try {
      const response = await axios.get(BASE_URL + "/api/nri/getAllNriServices", {
        responseType: 'json',
      });
      const jsonData = response.data.body;
      if (!jsonData) {
        console.error('Error: API response does not contain valid data.');
        return;
      }
      const csvContent = convertJsonToCsv(jsonData);
      const blob = new Blob([csvContent], { type: 'text/csv;charset=utf-8;' });
      const link = document.createElement('a');
      const fileName = 'user_data.csv';
      link.href = window.URL.createObjectURL(blob);
      link.download = fileName;
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    } catch (error) {
      console.error('Error creating CSV:', error);
    }
  };
  
  // Function to convert JSON to CSV format
  const convertJsonToCsv = (jsonData) => {
    const header = Object.keys(jsonData[0]).join(',');
    const rows = jsonData.map(obj => Object.values(obj).join(','));
    return `${header}\n${rows.join('\n')}`;
  };
  return (
    <div>
      {isLoading && <Loaderscreen />}
      <Navbar />
      <div className="col-lg-12 side-div">
        <div>
          <Sidebar />
        </div>
        <div className="table-div">
        <div className="bbtn-div">
          <Button
              type="text"
              onClick={downloadData}
              style={{
                marginLeft: '20px',
                backgroundColor: "#294908",
                color: "#fff",
              }}
            >
              Download Data
            </Button>
          </div>
          <div>
            <Table
              columns={columns}
              dataSource={AllnriData}
              footer={() => (
                <div style={{ textAlign: 'right' }}>
                  <Dropdown overlay={Item}>
                    <Button>
                      {selectedEmployeeCodee ? ` ${selectedEmployeeCodee}` : 'Select Employee'}
                    </Button>
                  </Dropdown>
                  &nbsp;
                  <Button type="primary" style={{ marginRight: 8 }} onClick={()=>handleSubmitEmploy(empId)}>
                    Send
                  </Button>
                </div>
              )}
            />
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
};


export default page;
