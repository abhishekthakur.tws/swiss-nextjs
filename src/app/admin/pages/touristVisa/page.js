"use client";
import { TouristVisaListing, employeeList, userAssingToEmployee, userInfo } from "@/app/redux/authActions";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Navbar from "@/app/user/Components/Navbar";
import Sidebar from "../../adminComponents/sidebar";
import Footer from "@/app/user/Components/Footer";
import { Space, Table, Button, Dropdown, Menu, Checkbox } from "antd";
import Loaderscreen from "../../../user/Components/Loader"
import axios from 'axios';
import { BASE_URL } from "@/app/common/common";


const page = () => {
  const dispatch = useDispatch();
  const { TouristVisaList,employeeListData,isLoading } = useSelector((state) => state.slice);
  const [selectedEmployeeCodee, setSelectedEmployeeCodee] = useState(null);

  const [empData, setEmpData] = useState({
    user_assign_id: [],
    employeeCode: "",
    category:"tourist"
  });

  useEffect(() => {
    return(()=>{
      dispatch(TouristVisaListing());
      dispatch(employeeList())
    })
  }, [dispatch]);
  
  const columns = [
    {
      title: 'Full Name',
      dataIndex: 'fullName',
      key: 'fullName',
    },
    {
      title: 'Father Name',
      dataIndex: 'fatherName',
      key: 'fatherName',
    },
    {
      title: 'Address',
      dataIndex: 'address',
      key: 'address',
    },
    {
      title: 'Current Country',
      dataIndex: 'currentCountry',
      key: 'currentCountry',
    },
    {
      title: 'Destination Country',
      dataIndex: 'destinationCountry',
      key: 'destinationCountry',
    },
  {
    title: "Select",
    key: "select",},
    {
      render: (_, record) => (
        <Space size="middle">
          <Checkbox onChange={(e) => handleCheckboxChange(record.id, e.target.checked)} />
        </Space>
      ),
    },
  ];

  const handleSubmitEmploy = () => {
    dispatch(userAssingToEmployee(empData));
  };


  const handleCheckboxChange = (key, checked) => {
    if (checked) {
      setEmpData((prevData) => ({
        ...prevData,
        user_assign_id: [...prevData.user_assign_id, key],
      }));
    } else {
      setEmpData((prevData) => ({
        ...prevData,
        user_assign_id: prevData.user_assign_id.filter((existingKey) => existingKey !== key),
      }));
    }
  };

  const empId = (selectedValue) => {
    setSelectedEmployeeCodee(selectedValue)
    setEmpData((prevData) => ({
      ...prevData,
      employeeCode: selectedValue
    }));
  };

  const menu = (
    <Menu>
      {employeeListData.map((data) => (
        <Menu.Item key={data.id} onClick={() => empId(data.employeeCode)}>
          {data.employeeCode}
        </Menu.Item>
      ))}
    </Menu>
  );


  const downloadData = async () => {
    try {
      const response = await axios.get(BASE_URL + "/api/tourist/gettouristVisaInfo", {
        responseType: 'json',
      });
      const jsonData = response.data.body;
      if (!jsonData) {
        console.error('Error: API response does not contain valid data.');
        return;
      }
      const csvContent = convertJsonToCsv(jsonData);
      const blob = new Blob([csvContent], { type: 'text/csv;charset=utf-8;' });
      const link = document.createElement('a');
      const fileName = 'user_data.csv';
      link.href = window.URL.createObjectURL(blob);
      link.download = fileName;
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    } catch (error) {
      console.error('Error creating CSV:', error);
    }
  };
  
  // Function to convert JSON to CSV format
  const convertJsonToCsv = (jsonData) => {
    const header = Object.keys(jsonData[0]).join(',');
    const rows = jsonData.map(obj => Object.values(obj).join(','));
    return `${header}\n${rows.join('\n')}`;
  };

  return (
    <div>
    {isLoading && <Loaderscreen/>}
    <Navbar />
    <div className="col-lg-12 side-div">
      <div>
        <Sidebar />
      </div>
      <div className="table-div">
      <div className="bbtn-div">
      <Button
          type="text"
          onClick={downloadData}
          style={{
            marginLeft: '20px',
            backgroundColor: "#294908",
            color: "#fff",
          }}
        >
          Download Data
        </Button>
      </div>
      <Table columns={columns} dataSource={TouristVisaList} 
       footer={() => (
        <div style={{ textAlign: 'right' }}>
          <Dropdown overlay={menu}>
            <Button>
              {selectedEmployeeCodee ? ` ${selectedEmployeeCodee}` : 'Select Employee'}
            </Button>
          </Dropdown>
          &nbsp;
          <Button type="primary" style={{ marginRight: 8 }} onClick={() => handleSubmitEmploy(empId)}>
            Send
          </Button>
        </div>
      )}/>
      </div>
      </div>
      <Footer />
    </div>
  )
};

export default page;
