"use client";
import { usePathname } from "next/navigation";
import React from "react";

  const Sidebar = () => {

    const pathname = usePathname();
    const isActive = (targetPath) => {
      return pathname === targetPath;
    };
    return (
      <div className="sidebar">
        <ul className="navbar-nav me-auto mb-2 mb-lg-0">
        <li className={`nav-item ${isActive('/admin/pages/dashboard') ? 'active' : ''}`}>
            <a href="/admin/pages/dashboard" className="nav-link" aria-current="page">
              🏠 Home
            </a>
          </li>
          <li className={`nav-item ${isActive("/admin/pages/nriServices") ? "active" : ""}`}>
            <a href="/admin/pages/nriServices" className="nav-link">
              🌐 NRI Services List
            </a>
          </li>
          <li className={`nav-item ${isActive("/admin/pages/touristVisa") ? "active": ""}`}>
            <a href="/admin/pages/touristVisa" className="nav-link">
              ✈️ Tourist Visa List
            </a>
          </li>
          <li className={`nav-item ${isActive("/admin/pages/studentVisa") ? "active": ""}`}>
            <a href="/admin/pages/studentVisa" className="nav-link">
              🎓 Student Visa List
            </a>
          </li>
          <li className={`nav-item ${isActive("/admin/pages/workPermit") ? "active": ""}`}>
            <a href="/admin/pages/workPermit" className="nav-link">
              🛠️ Work Permit List
            </a>
          </li>
          <li className={`nav-item ${isActive("/admin/pages/employeeData") ? "active": ""}`}>
            <a href="/admin/pages/employeeData" className="nav-link">
            🧑‍💼 Employee Data
            </a>
          </li>
        </ul>
      </div>
    );
  };

export default Sidebar;
