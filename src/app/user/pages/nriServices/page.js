"use client";
import React, { useEffect, useState } from "react";
import NRIServicesPic from "../../../assets/images/nri-services.jpg";
import Image from "next/image";
import Navbar from "../../Components/Navbar";
import Footer from "../../Components/Footer";
import { useDispatch, useSelector } from "react-redux";
import { ServicesListing, addNriDetails } from "@/app/redux/authActions";
import ServicesData from "../../../../app/redux/authslice";

const NriServices = () => {
  const { ServicesData } = useSelector((state) => state.slice);
  const dispatch = useDispatch();

  const [nridata, setNridata] = useState({
    name: "",
    service_id: "",
    contact_no: "",
  });

  useEffect(() => {
    return () => {
      dispatch(ServicesListing());
    };
  }, []);

  const handleSelect = (option) => {
    setNridata((prevData) => ({
      ...prevData,
      service_id: option,
    }));
  };

  const handleChange = (e) => {
    e.preventDefault();
    const { id, value } = e.target;
    setNridata((prevData) => ({
      ...prevData,
      [id]: value,
    }));
  };

  const handleSubmit = () => {
    dispatch(addNriDetails(nridata));
    setNridata({
      name: "",
      service_id: "",
      contact_no: "",
    });
  };

  return (
    <div>
      <Navbar />
      <section className="visa-sec">
        <div className="container">
          <div className="row">
          <div className="col-lg-6" style={{ marginBottom: "20px" }}>
              <Image src={NRIServicesPic} alt="nri-service-visa" style={{ height: "100%", width: "100%" }} />
            </div>
            <div className="col-lg-6" style={{ marginBottom: "20px", marginTop: "25px" }}>
              <h2>UNDERSTANDING THE SIGNIFICANCE OF A NRI SERVICES</h2>
              <p style={{ marginBottom: "5rem" }}>
                NRI services often assist in managing properties owned by non-residents in their home country. NRI services offer assistance in banking, investments, repatriation
                of funds, and managing assets in compliance with regulations both in the home country and the country of residence.NRI services may include guidance on health
                insurance options, hospitals, and medical facilities, ensuring adequate coverage and support during health emergencies.NRI services might facilitate processes
                related to guardianship, inheritance, legal matters, and educational support for dependents.NRI services often provide guidance on documentation, legal formalities,
                power of attorney, visas, and other bureaucratic requirements, ensuring compliance with laws and regulations. NRI services often offer insights into local market
                trends, real estate, stocks, and other investment avenues, helping them make informed decisions.
              </p>
            </div>

            <div style={{ marginBottom: "20px", marginTop: "25px" }}>
              <h2>YOUR PATHWAY TO OVERSEAS RESIDENCY - NRI SERVICES TAILORED FOR YOU</h2>
              <hr />
              <p style={{ marginTop: "3rem", marginBottom: "3rem" }}>
                Delve into invaluable insights and essential guidance for accessing NRI services, enabling you to embark on a new journey, pursue career prospects, reunite with
                family, or embrace a fresh environment. Navigating the complexities of NRI services is pivotal for a seamless transition and a thriving life in a new country.
              </p>
              <hr />
              <section className="service-div">
                <div className="container text-center">
                  <div className="row">
                    <div className="col-lg-6">
                      <span className="service-heading">Nri Services</span>
                      <select value={nridata.service_id} className="select-div" onChange={(e) => handleSelect(e.target.value)}>
                        <option value="" disabled>
                          Select an option
                        </option>
                        {ServicesData.map((data) => (
                          <option key={data.id}>{data.service_name}</option>
                        ))}
                      </select>
                    </div>
                    <div className="col-lg-6">
                      <form>
                        <div className="form-group">
                          <label htmlFor="name">Name</label>
                          <input type="text" className="form-control" id="name" value={nridata.name} onChange={handleChange} />
                        </div>
                        <div className="form-group">
                          <label htmlFor="contactNo">Contact No</label>
                          <input type="text" className="form-control" id="contact_no" value={nridata.contact_no} onChange={handleChange} />
                        </div>
                        <div className="nri-submit">
                          <button type="button" className="btn btn-success" onClick={() => handleSubmit(handleSelect)}>
                            Submit
                          </button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </section>

      <Footer />
    </div>
  );
};

export default NriServices;
