"use client";
import React from "react";
import touristvisa from "../../../assets/images/touristvisa.jpg";
import Image from "next/image";
import Navbar from "../../Components/Navbar";
import Footer from "../../Components/Footer";
import "../../../../app/globals.css";
import TouristForm from "../../Components/TouristForm";

const TouristandVisitorVisa = () => {
 

  return (
    <div>
      <Navbar />
      <section className="visa-sec">
        <div className="container">
          <div className="row">
            <div>
              <h2>EXPLORING TOURIST AND VISITOR VISAS</h2>
              <hr />
            </div>
            <div className="col lg 6">
              <Image src={touristvisa} alt="tourist-visa" style={{ height: "100%", width: "100%" }} />
            </div>
            <div className="col-lg-6">
              <p>
                Welcome to IMMIGRATION LAW CONSULTANCY, where we understand the excitement and intricacies of planning a trip to a new country. Whether you're a tourist eager to
                explore our country's wonders or a visitor attending special events or visiting family and friends, we're dedicated to ensuring your journey is seamless and
                stress-free. Our expertise lies in aiding with tourist and visitor visas, providing comprehensive support to secure the right visa for your travel purposes.
                Leveraging our profound understanding of visa procedures, our goal is to streamline the application process, offering unwavering guidance at every phase of your
                visa application.
              </p>
            </div>
            <div>
              <hr />
              <h2>What Sets Us Apart for Your Tourist and Visitor Visa Requirements?</h2>
              <hr />
            </div>
            <ul>
              <li style={{ marginBottom: "10px" }}>
                <b>Expertise at Your Service:</b> Our team of seasoned visa consultants is deeply familiar with the intricacies of tourist and visitor visa requisites and
                procedures. We stay abreast of the latest regulations, offering you precise and dependable guidance throughout the application process.
              </li>
            </ul>
          </div>
        </div>
      </section>
<TouristForm/>
      
      <Footer />
    </div>
  );
};

export default TouristandVisitorVisa;
