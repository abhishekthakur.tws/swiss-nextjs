"use client";
import React from "react";
import Navbar from "../../Components/Navbar";
import Footer from "../../Components/Footer";
import "./ourServices.css";
import Image from "next/image";
import nriPic from "../../../assets/images/nri-services-pic.png";
import studentVisa from "../../../assets/images/student-visa-pic.png";
import touristVisa from "../../../assets/images/tourist-visa-pic.png";
import workVisa from "../../../assets/images/work-permit-pic.png";
import { useRouter } from "next/navigation";

const OurServices = () => {
  const router = useRouter();
  return (
    <>
      <Navbar />
      <section id="services" className="bg-light pt-100 pb-70">
        <div className="container">
          <div className="row align-items-center">
            <div className="col-lg-5">
              <h2 className="title">
                What <span>Services</span> We Offer
              </h2>
              <p className=" mt-3 ">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut optio velit inventore, expedita quo laboriosam possimus ea consequatur vitae, doloribus consequuntur
                ex. Nemo assumenda laborum vel, labore ut velit dignissimos.
              </p>
              {/* <a className="btn btn-primary my-5" href="#">
                More Info{" "}
              </a> */}
            </div>
            <div className="col-lg-7">
              <div className="row card-items">
                <div className="col-lg-6 col-sm-6">
                  <div className="card">
                    <div className="card-body">
                      <Image src={studentVisa} alt="studentVisa" width={40} height={40} />
                      <h5 className="card-title">Student Visa</h5>
                      <p className="card-text">With supporting text below as a natural lead-in to additional content.</p>
                      <a className="btn btn-primary" href="#" onClick={() => router.push("/user/pages/studentVisa")}>
                        Apply Here{" "}
                      </a>
                    </div>
                  </div>
                </div>
                <div className="col-lg-6 col-sm-6">
                  <div className="card">
                    <div className="card-body">
                      <Image src={touristVisa} alt="tourist-visa" width={40} height={40} />
                      <h5 className="card-title">Tourist Visa</h5>
                      <p className="card-text">With supporting text below as a natural lead-in to additional content.</p>
                      <a className="btn btn-primary" href="/admin/pages/touristAndVisitorVisa">
                        Apply Here{" "}
                      </a>
                    </div>
                  </div>
                </div>
                <div className="col-lg-6 col-sm-6">
                  <div className="card">
                    <div className="card-body">
                      <Image src={workVisa} alt="workVisa" width={40} height={40} />
                      <h5 className="card-title">Work Permits</h5>
                      <p className="card-text">With supporting text below as a natural lead-in to additional content.</p>
                      <a className="btn btn-primary" href="#">
                        Apply Here{" "}
                      </a>
                    </div>
                  </div>
                </div>
                <div className="col-lg-6 col-sm-6">
                  <div className="card">
                    <div className="card-body">
                      {/* <i className="bi bi-phone"></i> */}
                      <Image src={nriPic} alt="nriPic" width={40} height={40} />
                      <h5 className="card-title"> NRI Services</h5>
                      <p className="card-text">With supporting text below as a natural lead-in to additional content.</p>
                      <a className="btn btn-primary" href="#">
                        Apply Here{" "}
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <Footer />
    </>
  );
};

export default OurServices;
