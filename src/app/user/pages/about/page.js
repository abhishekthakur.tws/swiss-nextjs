"use client";
import React, { useState, useEffect } from "react";
import LoadingScreen from "../../Components/Loader";
import Navbar from "../../Components/Navbar";
import Footer from "../../Components/Footer";
import "./about.css";
import background from "../../../assets/images/background.jpg";
import MeeetTheTeam from "../../../assets/images/Meet-the-team.jpg";
import DevPic from "../../../assets/images/dev.jpg";
import SanjeevPic from "../../../assets/images/sanjeev.jpg";
import VarunPic from "../../../assets/images/Varun.jpg";
import Image from "next/image";

const About = () => {
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    setTimeout(() => setLoading(false), 3000);
  });
  return (
    <>
      {/* {loading && <LoadingScreen />} */}
      <Navbar />
      <div className="main">
        <section className="container text-center company-name">
          <div className="row">
            <div className="col-lg-12 about-content">
              <h1>About Swiis</h1>
            </div>
          </div>
        </section>

        <section className="container text-center">
          <div className="row">
            <div className="col-lg-12">
              <h3 className="content-head">What we do</h3>
              <p className="content-para">
                Swiis offer an unrivalled understanding of the healthcare sector with considerable experience of placing high-calibre healthcare and support staff into work when
                and where they want.
                {/* Swiis International Ltd is the parent company of Swiis Foster Care England, Swiis Foster Care Scotland and Swiis Healthcare. With a wealth of experience in foster,
                health, and social care, we have a proven track record and a reputation for excellence. */}
              </p>
              {/* <h3 className="content-head">Our History</h3>
              <p className="content-para">
                Swiis was founded in 1988 by our Chairman and owner Mr Dev Dadral initially providing local social care provision from an office in Ealing, West London. The Swiis
                commitment to quality was soon recognised by our clients, enabling the Swiis business to grow extensively over the years, diversifying out into foster care
                (England) in 2000, healthcare in 2004 and foster care (Scotland) in 2005.
              </p> */}
            </div>
          </div>
        </section>

        <section className="container text-center">
          <div className="row">
            <div className="col-lg-12">
              <h3 className="content-head">Our Services</h3>
            </div>
          </div>
        </section>

        <section className="container text-center">
          <div className="row">
            <div className="col-lg-6 img-div">
              <ul>
                <li>
                  <Image src={DevPic} className="img-radius" height={100} width={100} alt="Mr. Dev" />
                  <span className="names">Mr. Dev Dadral</span>
                  <span>Managing Director</span>
                </li>
                <li>
                  <Image src={SanjeevPic} className="img-radius" height={100} width={100} alt="Mr. Sanjeev" />
                  <span className="names">Mr. Sanjeev Kumar</span>
                  <span>Director of Operations</span>
                </li>
                <li>
                  <Image src={VarunPic} className="img-radius" height={100} width={100} alt="Mr. Varun" />
                  <span className="names">Mr. Varun Vasudeva</span>
                  <span>Director of Services</span>
                </li>
              </ul>
            </div>
            <div className="col-lg-6">
              <h5 className="service-head">1. Expert Guidance:</h5>
              <p className="content-para">
                Navigating the complex world of immigration, visas, and international studies can be overwhelming. Our team of seasoned experts is here to guide you through every
                step of the process. We stay updated on the latest regulations and requirements to ensure your application is handled with precision.
              </p>

              <h5 className="service-head">2. Personalized Solutions:</h5>
              <p className="content-para">
                Recognizing that every individual's situation is unique, we offer personalized solutions that cater to your specific goals and circumstances. Whether you're looking
                to reunite with family, explore new travel opportunities, or embark on an educational journey, we have the expertise to make it happen.
              </p>

              <h5 className="service-head">3. Transparency and Integrity:</h5>
              <p className="content-para">
                Transparency is the foundation of our service. We believe in keeping you informed at every stage, providing clear communication and honest advice. Our commitment to
                integrity ensures that you can trust us to handle your case with the utmost professionalism.
              </p>
            </div>
          </div>
        </section>

        <section className="container text-center">
          <div className="row">
            <div className="col-lg-12">
              <h3 className="content-head">Meet the team</h3>
            </div>
          </div>
        </section>

        <section className="container text-center">
          <div className="row">
            <div className="col-lg-6">
              <p className="content-para-next">
                Our recruitment team is comprised of highly skilled professionals who undergo rigorous training, possess exceptional qualifications, and boast extensive experience
                in the field.
                <p />
                <p className="content-para-next">
                  Our office-based team members are dedicated to upholding our core philosophy, which centers transparency, honesty, and utmost respect in all interactions with
                  both our clients and healthcare staff. Our commitment to excellence is evident in the continuous development and training programs that our recruitment team
                  undergoes.
                </p>
                <p className="content-para-next">
                  Our office-based team members are dedicated to upholding our core philosophy, which centers transparency, honesty, and utmost respect in all interactions with
                  both our clients and healthcare staff. Our commitment to excellence is evident in the continuous development and training programs that our recruitment team
                  undergoes.
                </p>
                <p className="content-para-next">
                  Our team stays updated on industry trends, and regulations, ensuring top-tier services. We not only meet but exceed industry standards, fostering a collaborative
                  partnership beyond recruitment. Effective communication and transparency are at the core of our values, making our team ambassadors and quality service. We aim to
                  connecting qualified professionals with institutions and organizations in need.
                </p>
              </p>
              {/* <h5 className="service-head">1. Immigration Services:</h5>
              <p className="content-para">
                Embarking on a new life in a different country is a monumental decision. Our immigration services cover a wide range of needs, including family reunification,
                employment-based immigration, and assistance with permanent residency applications. We are dedicated to making your transition as smooth as possible.{" "}
              </p>

              <h5 className="service-head">2. Tourist Visa Services:</h5>
              <p className="content-para">
                Travel opens up a world of possibilities, and our tourist visa services are designed to make your journey hassle-free. Whether you're planning a leisurely vacation,
                a business trip, or a cultural exploration, we assist you in securing the necessary visas with efficiency and precision.{" "}
              </p>

              <h5 className="service-head">3. Study Visa Services:</h5>
              <p className="content-para">
                Education knows no borders, and our study visa services are crafted to help you pursue academic excellence anywhere in the world. From selecting the right
                educational institution to handling the visa application process, we provide comprehensive support to aspiring students.{" "}
              </p> */}
            </div>
            <div className="col-lg-6">
              <Image src={MeeetTheTeam} height={500} width={540} alt="imigration-image" />
            </div>
          </div>
        </section>
      </div>
      <Footer />
    </>
  );
};

export default About;
