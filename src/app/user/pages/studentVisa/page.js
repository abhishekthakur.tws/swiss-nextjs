"use client";
import React from "react";
import StudentVisas from "../../../assets/images/studentvisa.png";
import Image from "next/image";
import "./studentVisa.css";
import Navbar from "../../Components/Navbar";
import Footer from "../../Components/Footer";
import StudentVisaForm from "../../Components/StudentVisaForm";

const StudentVisa = () => {

  return (
    <div>
      <Navbar />
      <section className="visa-sec">
        <div className="container">
          <div className="row">
            <div style={{ marginBottom: "20px", marginTop: "25px" }}>
              <hr />
              <h2>Your Ultimate Resource for Student Visa Information</h2>
              <hr />
              <p>
                Embarking on an educational journey in a foreign land requires the pivotal document of a student visa, granting you the legal authorization to study and reside
                within your chosen destination. Studying abroad presents a wealth of advantages, from cultural immersion to access to esteemed educational institutions, fostering
                an expansive worldview. Yet, navigating the complexities of securing a student visa can be daunting and time-intensive. That's where we step in, committed to
                guiding you through each phase of the application process.
              </p>
            </div>
            <div className="col-lg-6">
              <Image src={StudentVisas} alt="student-visa" style={{ height: "90%", width: "100%" }} />
            </div>
            <div className="col-lg-6">
              <h2>Welcome to Your Student Visa Guide</h2>
              <p style={{ marginTop: "20px", marginBottom: "2rem" }}>
                Here, you'll discover invaluable insights into the multifaceted process of applying for a student visa. We'll lead you through the essential requirements, necessary
                documentation, application procedures, and key considerations crucial for a successful student visa application. It's essential to acknowledge that specific
                requirements and procedures can differ based on your chosen study destination. Hence, we encourage you to consult Immigration Law Consultancy for the latest,
                precise information pertinent to your intended country. Whether you're venturing into your academic journey for the first time or pursuing advanced studies, our aim
                is to furnish you with clear, concise guidance to navigate the student visa application process with confidence. We trust that this page will serve as a valuable
                resource, empowering you as you embark on your educational aspirations and pursue your student visa. Best wishes for a successful application and a fulfilling
                academic journey!
              </p>
            </div>
          </div>
        </div>
      </section>

     <StudentVisaForm/>
      <Footer />
    </div>
  );
};

export default StudentVisa;
