"use client";
import React, { useState } from "react";
import Navbar from "../../Components/Navbar";
import Footer from "../../Components/Footer";
import WorkPermitPic from "../../../assets/images/work_&_settle_in_uk.webp";
import { Formik, Field, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import { useDispatch } from "react-redux";
import { workPermitInfo } from "../../../redux/authActions";
import Image from "next/image";

const page = () => {
  const dispatch = useDispatch();
  const [workPermitFields, setWorkPermitFields] = useState({
    fullName: "",
    fatherName: "",
    dateOfBirth: "",
    gender: "",
    nationality: "",
    maritalStatus: "",
    address: "",
    email: "",
    phoneNumber: "",
    academicQualifications: "",
    degree: "",
    passportNumber: "",
    jobTitle: "",
    jobDescription: "",
    employerName: "",
    durationOfEmployment: "",
    salaryAndOtherCompensationDetails: "",
    criminalBackground: "",
    supportingDocuments: "",
    englishProficiencyTestScores: "",
    bankStatements: "",
    healthInsuranceDetails: "",
    passportSizedPhotographs: "",
    scannedDocuments: "",
    professionalCertifications: "",
  });

  const [formErrors, setFormErrors] = useState({});

  const validateForm = () => {
    const errors = {};
    if (!workPermitFields.fullName) {
      errors.fullName = "Required *";
    }
    if (!workPermitFields.fatherName) {
      errors.fatherName = "Required *";
    }
    if (!workPermitFields.dateOfBirth) {
      errors.dateOfBirth = "Required *";
    }
    if (!workPermitFields.gender) {
      errors.gender = "Required *";
    }
    if (!workPermitFields.nationality) {
      errors.nationality = "Required *";
    }
    if (!workPermitFields.maritalStatus) {
      errors.maritalStatus = "Required *";
    }
    if (!workPermitFields.address) {
      errors.address = "Required *";
    }
    if (!workPermitFields.email) {
      errors.email = "Required *";
    }
    if (!workPermitFields.phoneNumber) {
      errors.phoneNumber = "Required *";
    }
    if (!workPermitFields.academicQualifications) {
      errors.academicQualifications = "Required *";
    }
    setFormErrors(errors);
    return Object.keys(errors).length === 0;
  };

  const validationSchema = Yup.object().shape({
    fullName: Yup.string().required("required *"),
    fatherName: Yup.string().required("required *"),
    dob: Yup.string().required("required *"),
    gender: Yup.string().required("required *"),
    nationality: Yup.string().required("required *"),
    maritalStatus: Yup.string().required("required *"),
    address: Yup.string().required("required *"),
    emailAddress: Yup.string().required("required *"),
    phoneNumber: Yup.string().required("required *"),
    academicQualifications: Yup.string().required("required *"),
    degree: Yup.string().required("required *"),
    passportNumber: Yup.string().required("required *"),
  });

  const handleInputChange = (e) => {
    const { name, value, files, type } = e.target;
    if (type === "file") {
      setWorkPermitFields((prevData) => ({
        ...prevData,
        [name]: files[0],
      }));
    } else {
      setWorkPermitFields((prevData) => ({
        ...prevData,
        [name]: value,
      }));
    }
    validateForm();
  };

  const formSubmit = (e) => {
    e.preventDefault();
    if (validateForm()) {
      dispatch(workPermitInfo(workPermitFields));
      setWorkPermitFields({
        fullName: "",
        fatherName: "",
        dateOfBirth: "",
        gender: "",
        nationality: "",
        maritalStatus: "",
        address: "",
        email: "",
        phoneNumber: "",
        academicQualifications: "",
        degree: "",
        passportNumber: "",
        jobTitle: "",
        jobDescription: "",
        employerName: "",
        durationOfEmployment: "",
        salaryAndOtherCompensationDetails: "",
        criminalBackground: "",
        supportingDocuments: "",
        englishProficiencyTestScores: "",
        bankStatements: "",
        healthInsuranceDetails: "",
        passportSizedPhotographs: "",
        scannedDocuments: "",
        professionalCertifications: "",
      });
    }
  };

  const clearForm = (resetForm) => {
    resetForm();
  };

  return (
    <>
      <Navbar />
      <section className="work-permit">
        <Image src={WorkPermitPic} alt="work permit pic" />
        <div className="row" style={{ marginTop: "50px" }}>
          <div className="col-lg-6">
            <h2>Why Apply for UK Tier-2 Visa?</h2>
            <ul>
              <li>Work in UK for 5 years.</li>
              <li>Get a faster decision on your application.</li>
              <li>Best route to migrate to UK.</li>
            </ul>
          </div>
          <div className="col-lg-6">
            <h2>Work & Settle in UK</h2>
            <p>
              In order to retain its competitive edge, the UK invites skilled professionals to work in the UK under the Tier 2 visa program. Under this program, workers whose
              occupations are listed on the Tier 2 Shortage Occupation List can apply to work in the UK on a long-term basis. Among the popular professions on the list are IT,
              finance, teaching, healthcare, and engineering. Y-Axis can help you take advantage of this talent shortage in the UK and position yourself to gain a work permit to
              the UK.
              <br />
              If skilled workers must come to the UK, they need to have a Skilled Worker visa, (formerly the Tier 2 visa). You can apply for this visa if you have been offered a
              skilled job in the UK. The salary requirement for this visa is £25,600, or the specific salary requirement for the occupation or the ‘going rate’.
            </p>
          </div>
        </div>
      </section>

      <div className="form">
        <section className="container formInfo">
          <div className="row">
            <div className="col-lg-12">
              <h1 className="info-text" style={{ fontFamily: "sans-serif" }}>
                Please enter the following details <br />
              </h1>
            </div>
            <div className="col-lg-12">
              <h2 style={{ color: "#294908", fontFamily: "sans-serif" }}>For Work - Visa Application process</h2>
            </div>
            <Formik
              initialValues={{
                fullName: "",
                fatherName: "",
                dateOfBirth: "",
                gender: "",
                nationality: "",
                maritalStatus: "",
                address: "",
                email: "",
                phoneNumber: "",
                academicQualifications: "",
                degree: "",
                passportNumber: "",
                jobTitle: "",
                jobDescription: "",
                employerName: "",
                durationOfEmployment: "",
                salaryAndOtherCompensationDetails: "",
                criminalBackground: "",
                supportingDocuments: "",
                englishProficiencyTestScores: "",
                bankStatements: "",
                healthInsuranceDetails: "",
                passportSizedPhotographs: "",
                scannedDocuments: "",
                professionalCertifications: "",
              }}
            >
              {({ errors, touched, setFieldTouched }) => (
                <Form onSubmit={formSubmit}>
                  <div className="container border border-dark mb-3 rounded submit-form">
                    <div className="row form_content">
                      <div className="col-lg-4">
                        <label htmlFor="name" className="form-label">
                          Full Name
                        </label>
                        <Field
                          type="text"
                          name="fullName"
                          id="fullName"
                          className="fields-width"
                          placeholder="Enter your fullName"
                          value={workPermitFields.fullName}
                          onChange={handleInputChange}
                        />
                        {formErrors.fullName && (
                          <span className="error" style={{ color: "red" }}>
                            {formErrors.fullName}
                          </span>
                        )}
                      </div>
                      <div className="col-lg-4">
                        <label htmlFor="name" className="form-label">
                          Father Name
                        </label>
                        <Field
                          type="text"
                          name="fatherName"
                          id="fatherName"
                          className="fields-width"
                          placeholder="Enter your fatherName"
                          value={workPermitFields.fatherName}
                          onChange={handleInputChange}
                        />
                        {formErrors.fatherName && (
                          <span className="error" style={{ color: "red" }}>
                            {formErrors.fatherName}
                          </span>
                        )}
                      </div>
                      <div className="col-lg-4">
                        <label htmlFor="dateOfBirth" className="form-label">
                          Date Of Birth
                        </label>
                        <Field
                          type="date"
                          name="dateOfBirth"
                          id="dateOfBirth"
                          className="fields-width"
                          placeholder="Enter your Date Of Birth"
                          value={workPermitFields.dateOfBirth}
                          onChange={handleInputChange}
                        />
                        {formErrors.dateOfBirth && (
                          <span className="error" style={{ color: "red" }}>
                            {formErrors.dateOfBirth}
                          </span>
                        )}
                      </div>
                      <div className="col-lg-4">
                        <label htmlFor="gender" className="form-label">
                          Gender
                        </label>
                        <Field
                          as="select"
                          onChange={handleInputChange}
                          value={workPermitFields.gender}
                          name="gender"
                          id="gender"
                          className="fields-width gender"
                        >
                          <option value="" label="Select your gender" />
                          <option value="male" label="Male" />
                          <option value="female" label="Female" />
                          <option value="other" label="Other" />
                        </Field>
                        {formErrors.gender && (
                          <span className="error" style={{ color: "red" }}>
                            {formErrors.gender}
                          </span>
                        )}
                      </div>

                      <div className="col-lg-4">
                        <label htmlFor="nationality" className="form-label">
                          Nationality
                        </label>
                        <Field
                          type="text"
                          name="nationality"
                          id="nationality"
                          className="fields-width"
                          placeholder="Enter your Nationality"
                          value={workPermitFields.nationality}
                          onChange={handleInputChange}
                        />
                        {formErrors.nationality && (
                          <span className="error" style={{ color: "red" }}>
                            {formErrors.nationality}
                          </span>
                        )}
                      </div>
                      <div className="col-lg-4">
                        <label htmlFor="maritalStatus" className="form-label">
                          Marital Status
                        </label>
                        <Field
                          as="select"
                          name="maritalStatus"
                          id="maritalStatus"
                          className="fields-width gender"
                          value={workPermitFields.maritalStatus}
                          onChange={handleInputChange}
                        >
                          <option value="" label="Select your Status" />
                          <option value="Single" label="Single" />
                          <option value="Divorced" label="Divorced" />
                          <option value="Married" label="Married" />
                        </Field>
                        {formErrors.maritalStatus && (
                          <span className="error" style={{ color: "red" }}>
                            {formErrors.maritalStatus}
                          </span>
                        )}
                      </div>

                      <div className="col-lg-4">
                        <label htmlFor="email" className="form-label">
                          Email Address
                        </label>
                        <Field
                          type="email"
                          name="email"
                          id="email"
                          className="fields-width"
                          placeholder="Enter your email"
                          value={workPermitFields.email}
                          onChange={handleInputChange}
                        />
                        {formErrors.email && (
                          <span className="error" style={{ color: "red" }}>
                            {formErrors.email}
                          </span>
                        )}
                      </div>

                      <div className="col-lg-4">
                        <label htmlFor="address" className="form-label">
                          Address
                        </label>
                        <Field
                          type="text"
                          name="address"
                          id="address"
                          className="fields-width"
                          placeholder="Enter your address"
                          value={workPermitFields.address}
                          onChange={handleInputChange}
                        />
                        {formErrors.address && (
                          <span className="error" style={{ color: "red" }}>
                            {formErrors.address}
                          </span>
                        )}
                      </div>

                      <div className="col-lg-4">
                        <label htmlFor="academic_qualifications" className="form-label">
                          Academic Qualifications
                        </label>
                        <Field
                          type="text"
                          name="academicQualifications"
                          id="academicQualifications"
                          className="fields-width"
                          onChange={handleInputChange}
                          placeholder="Enter your Academic Qualifications "
                          value={workPermitFields.academicQualifications}
                        />
                        {formErrors.academicQualifications && (
                          <span className="error" style={{ color: "red" }}>
                            {formErrors.academicQualifications}
                          </span>
                        )}
                      </div>

                      <div className="col-lg-4">
                        <label htmlFor="academic_qualifications" className="form-label">
                          Phone Number
                        </label>
                        <Field
                          type="text"
                          name="phoneNumber"
                          id="phoneNumber"
                          className="fields-width"
                          onChange={handleInputChange}
                          placeholder="Enter your phone number"
                          value={workPermitFields.phoneNumber}
                        />
                        {formErrors.phoneNumber && (
                          <span className="error" style={{ color: "red" }}>
                            {formErrors.phoneNumber}
                          </span>
                        )}
                      </div>

                      <div className="col-lg-4">
                        <label htmlFor="degree" className="form-label">
                          Degree
                        </label>
                        <Field
                          type="text"
                          name="degree"
                          id="degree"
                          className="fields-width"
                          placeholder="Enter your Degree"
                          onChange={handleInputChange}
                          value={workPermitFields.degree}
                        />
                      </div>

                      <div className="col-lg-4">
                        <label htmlFor="passportNumber" className="form-label">
                          Passport Number
                        </label>
                        <Field
                          type="text"
                          name="passportNumber"
                          id="passportNumber"
                          className="fields-width"
                          placeholder="Enter your Passport Number"
                          onChange={handleInputChange}
                          value={workPermitFields.passportNumber}
                        />
                      </div>

                      <div className="col-lg-4">
                        <label htmlFor="jobTitle" className="form-label">
                          Job Title
                        </label>
                        <Field
                          type="text"
                          name="jobTitle"
                          id="jobTitle"
                          className="fields-width"
                          placeholder="Enter your Job title"
                          onChange={handleInputChange}
                          value={workPermitFields.jobTitle}
                        />
                      </div>
                      <div className="col-lg-4">
                        <label htmlFor="jobDescription" className="form-label">
                          Job Description
                        </label>
                        <Field
                          type="text"
                          name="jobDescription"
                          id="jobDescription"
                          className="fields-width"
                          placeholder="Enter your Job Description"
                          onChange={handleInputChange}
                          value={workPermitFields?.jobDescription}
                        />
                      </div>

                      <div className="col-lg-4">
                        <label htmlFor="employerName" className="form-label">
                          Employer Name
                        </label>
                        <Field
                          type="text"
                          name="employerName"
                          id="employerName"
                          className="fields-width"
                          placeholder="Enter employee name"
                          onChange={handleInputChange}
                          value={workPermitFields?.employerName}
                        />
                      </div>

                      <div className="col-lg-4">
                        <label htmlFor="durationOfEmployment" className="form-label">
                          Duration of Employment
                        </label>
                        <Field
                          type="text"
                          name="durationOfEmployment"
                          id="durationOfEmployment"
                          className="fields-width"
                          placeholder="Enter your employement duration"
                          onChange={handleInputChange}
                          value={workPermitFields?.durationOfEmployment}
                        />
                      </div>

                      <div className="col-lg-4">
                        <label htmlFor="salaryAndOtherCompensationDetails" className="form-label">
                          Salary and other details
                        </label>
                        <Field
                          type="text"
                          name="salaryAndOtherCompensationDetails"
                          id="salaryAndOtherCompensationDetails"
                          className="fields-width"
                          placeholder="Enter your salary and other Details"
                          onChange={handleInputChange}
                          value={workPermitFields?.salaryAndOtherCompensationDetails}
                        />
                      </div>

                      <div className="col-lg-4">
                        <label htmlFor="criminalBackground" className="form-label">
                          Criminal background details
                        </label>
                        <Field
                          type="text"
                          name="criminalBackground"
                          id="criminalBackground"
                          className="fields-width"
                          placeholder="Enter your criminal background"
                          onChange={handleInputChange}
                          value={workPermitFields?.criminalBackground}
                        />
                      </div>

                      <div className="col-lg-4">
                        <label htmlFor="supportingDocuments" className="form-label">
                          Supporting Documents
                        </label>
                        <input type="file" name="supportingDocuments" id="supportingDocuments" className="fields-width" placeholder="Documents" onChange={handleInputChange} />
                      </div>

                      <div className="col-lg-4">
                        <label htmlFor="englishProficiencyTestScores" className="form-label">
                          English test scores
                        </label>
                        <Field
                          type="text"
                          name="englishProficiencyTestScores"
                          id="englishProficiencyTestScores"
                          className="fields-width"
                          placeholder="English Test Scores"
                          onChange={handleInputChange}
                          value={workPermitFields?.englishProficiencyTestScores}
                        />
                      </div>

                      <div className="col-lg-4">
                        <label htmlFor="bankStatements" className="form-label">
                          Bank Statements
                        </label>
                        <input type="file" name="bankStatements" id="bankStatements" className="fields-width" placeholder="Bank statements" onChange={handleInputChange} />
                      </div>

                      <div className="col-lg-4">
                        <label htmlFor="healthInsuranceDetails" className="form-label">
                          Health Insurance Details
                        </label>
                        <input
                          type="file"
                          name="healthInsuranceDetails"
                          id="healthInsuranceDetails"
                          className="fields-width"
                          placeholder="Enter Health Insurance Details"
                          onChange={handleInputChange}
                        />
                      </div>

                      <div className="col-lg-4">
                        <label htmlFor="passportSizedPhotographs" className="form-label">
                          Passport Sized Photographs
                        </label>
                        <input
                          type="file"
                          name="passportSizedPhotographs"
                          id="passportSizedPhotographs"
                          className="fields-width"
                          placeholder="Enter Passport Sized Photographs"
                          onChange={handleInputChange}
                        />
                      </div>

                      <div className="col-lg-4">
                        <label htmlFor="scannedDocuments" className="form-label">
                          Scanned Documents
                        </label>
                        <input
                          type="file"
                          name="scannedDocuments"
                          id="scannedDocuments"
                          className="fields-width"
                          placeholder="Enter your Destination Country"
                          onChange={handleInputChange}
                        />
                      </div>

                      <div className="col-lg-4">
                        <label htmlFor="professionalCertifications" className="form-label">
                          Professional Certifications
                        </label>
                        <input
                          type="file"
                          name="professionalCertifications"
                          id="professionalCertifications"
                          className="fields-width"
                          placeholder="Enter your professional certifications"
                          onChange={handleInputChange}
                        />
                      </div>

                      <div className="col-lg-12 mb-2 d-flex align-item-end sbt-btn">
                        <button className="info-submit-form btn btn-secondary " type="submit" onClick={() => clearForm(resetForm)}>
                          Cancel
                        </button>
                        &nbsp;
                        <button className="info-submit-form" type="submit">
                          Submit
                        </button>
                      </div>
                    </div>
                  </div>
                </Form>
              )}
            </Formik>
          </div>
        </section>
      </div>
      <Footer />
    </>
  );
};

export default page;
