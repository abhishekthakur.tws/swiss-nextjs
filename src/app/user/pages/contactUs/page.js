"use client";
import React, { useEffect, useState } from "react";
import Navbar from "../../Components/Navbar";
import LoadingScreen from "../../Components/Loader";
import Footer from "../../Components/Footer";
import "./contact.css";

const Contactus = () => {
  const [loading, setloading] = useState(true);

  useEffect(() => {
    setTimeout(() => setloading(false), 2000);
  });

  return (
    <>
      {loading && <LoadingScreen />}
      <Navbar />
      <div className="container contact-main">
        <h1 className="text-center contact-heading">Contact us</h1>
        <br />

        <div className="row">
          <div className="col-md-8">
            <form action="/post" method="post">
              <input
                className="form-control"
                name="name"
                placeholder="Name..."
              />
              <br />
              <input
                className="form-control"
                name="phone"
                placeholder="Phone..."
              />
              <br />
              <input
                className="form-control"
                name="email"
                placeholder="E-mail..."
              />
              <br />
              <textarea
                className="form-control"
                name="text"
                placeholder="How can we help you?"
                style={{ height: "150px" }}
              ></textarea>
              <br />
              <input className="submit-btn" type="submit" value="Send" />
              <br />
              <br />
            </form>
          </div>
          <div className="col-md-4">
            <b style={{ color: "#3e6e0c", fontSize: "20px" }}>Contact Us :</b>{" "}
            <br />
            <span style={{ fontSize: "20px" }}>
              Swiis Consultants Private Limited{" "}
            </span>
            <br />
            <br />
            <b style={{ color: "#3e6e0c", fontSize: "20px" }}>Address :</b>
            <br />
            <span style={{ fontSize: "20px" }}>
              Street no. 4, Bank Colony, Mahilpur,
              <br />
              Hoshiarpur, Punjab{" "}
            </span>
            <br /> <span style={{ fontSize: "20px" }}>146105</span>
            <br />
            <br />
            <b style={{ color: "#3e6e0c", fontSize: "20px" }}>Phone Number :</b>
            <br />
            <span style={{ fontSize: "20px" }}>7527008800</span>
            <br />
            <br />
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
};

export default Contactus;
