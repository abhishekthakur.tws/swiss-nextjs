"use client";
import React, { useState, useEffect } from "react";
import Navbar from "../../Components/Navbar";
import Footer from "../../Components/Footer";
import "../../../../app/globals.css";
import InfoForm from "../../Components/InfoForm";
import LoadingScreen from "../../Components/Loader";

const page = () => {
  const [loading, setloading] = useState(true);

  // useEffect(() => {
  //   setTimeout(() => setloading(false), 2000);
  // });

  useEffect(() => {
    const timeoutId = setTimeout(() => {
      setloading(false);
    }, 2000);
    return () => clearTimeout(timeoutId);
  }, []);

  return (
    <>
      {/* {loading && <LoadingScreen />} */}
      <Navbar />
      <section className="container-fluid debt-management">
        <div className="row">
          <div className="col-lg-6 second-header">
            <h1 className="first-headings">
              OUR <br /> PROFILE
            </h1>
          </div>
          <div className="col-lg-6 second-header">
            <h2 className="second-headings">
              Helping People To <br /> Grow Better Together
            </h2>
          </div>
        </div>
        <div className="row" style={{ marginTop: "50px" }}>
          <h4 className="third-header">ABOUT US:</h4>
        </div>
        <div className="row">
          <div className="col-lg-4 text-center">
            <b className="same-font-size">OUR STORY</b>
            <p className="same-margin">
              Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an
              unknown printer took a galley of type and scrambled it to make a type specimen book.
            </p>
          </div>
          <div className="col-lg-4 text-center">
            <b className="same-font-size">OUR VISION</b>
            <p className="same-margin">
              Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an
              unknown printer took a galley of type and scrambled it to make a type specimen book.
            </p>
          </div>
          <div className="col-lg-4 text-center">
            <b className="same-font-size">OUR MISSION</b>
            <p className="same-margin">
              Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an
              unknown printer took a galley of type and scrambled it to make a type specimen book.
            </p>
          </div>
        </div>
        <div className="row style_css">
          <div className="col-lg-6 text-center">
            <b className="same-font-size ">OUR SERVICES</b>
            <ul>
              <li>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s printer took
                a galley to make a type specimen book.
              </li>
              <li>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s printer took
                a galley to make a type specimen book.
              </li>
              <li>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s printer took
                a galley to make a type specimen book.
              </li>
            </ul>
          </div>
          <div className="col-lg-6 text-center">
            <b className="same-font-size">WHY CHOOSE US ?</b>
            <ul>
              <li>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s printer took
                a galley to make a type specimen book.
              </li>
            </ul>
            <b className="same-font-size">Address</b>
            <p>
              {" "}
              STREET NO 4, BANK COLONY, MAHILPUR,
              <br /> HOSHIARPUR, PUNJAB, INDIA Pin - 146105
            </p>
            <b>CONTACT US</b>
            <p>+91 75270-08800 (Debt Rabbit)</p>
          </div>
        </div>
        <div className="row" style={{ marginBottom: "50px" }}>
          <div className="col-lg-12">
            <hr />
            <InfoForm />
          </div>
        </div>
      </section>
      <Footer />
    </>
  );
};

export default page;
