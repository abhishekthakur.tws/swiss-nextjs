"use client";
import React, { useState, useEffect } from "react";
import LoadingScreen from "../../Components/Loader";
import Navbar from "../../Components/Navbar";
import Footer from "../../Components/Footer";
import "./blog.css";
import Image from "next/image";
import Blog1 from "../../../assets/images/Blog1.jpg";
import Blog2 from "../../../assets/images/Blog2.jpg";
import Blog3 from "../../../assets/images/Blog3.jpg";

const page = () => {
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    setTimeout(() => setLoading(false), 3000);
  });

  return (
    <>
      {loading && <LoadingScreen />}
      <Navbar />
      <div className="container">
        <div className="row">
          <div className="col-lg-12">
            <h3>Blogs</h3>
          </div>
        </div>
      </div>
      <br />

      <div className="container">
        <div id="blog" className="row">
          <div className="col-md-10 blogShort text-start">
            <h1>VISA</h1>
            <Image src={Blog1} alt="blog-one"/>

            <article>
              <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an
                unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic
                typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently
                with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
              </p>
            </article>
            <a className="btn btn-blog pull-right marginBottom10" href="">
              READ MORE
            </a>
          </div>
          <div className="col-md-10 blogShort">
            <h1>SERVICES</h1>
            <Image src={Blog2} alt="blog-two" />
            <article>
              <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an
                unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic
                typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently
                with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
              </p>
            </article>
            <a className="btn btn-blog pull-right marginBottom10" href="">
              READ MORE
            </a>
          </div>

          <div className="col-md-10 blogShort">
            <h1>PROCEDURE</h1>
            <Image src={Blog3} alt="blog-three" />
            <article>
              <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an
                unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic
                typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently
                with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
              </p>
            </article>
            <a className="btn btn-blog pull-right marginBottom10" href="">
              READ MORE
            </a>
          </div>

          <div className="col-md-12 gap10"></div>
        </div>
      </div>

      <Footer />
    </>
  );
};

export default page;
