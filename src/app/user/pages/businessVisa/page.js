"use client";
import React, { useState, useEffect } from "react";
import BusinessVisas from "../../../assets/images/businessvisa.jpg";
import LoadingScreen from "../../Components/Loader";
import Image from "next/image";

const BusinessVisa = () => {
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    setTimeout(() => setLoading(false), 3000);
  });

  return (
    <div>
      {loading && <LoadingScreen />}
      <section className="visa-sec">
        <div className="container">
          <div className="row">
            <div style={{ marginBottom: "20px", marginTop: "25px" }}>
              <hr />
              <h2>BUSINESS VISA - Your Gateway to Business Travel</h2>
              <p>
                Greetings from Immigration Law Consultancy! We're thrilled about your interest in visiting our country for business endeavors. To streamline your travel and
                guarantee a seamless business trip, here's essential guidance on acquiring a business visa."
              </p>
              <hr />
            </div>
            <div className="col-lg-12" style={{ margin: "0px 0px 20px 0px" }}>
              <h2>INTRODUCING BUSINESS VISAS</h2>
            </div>
            <div className="col-lg-6">
              <Image src={BusinessVisas} alt="business-visa" style={{ height: "100%", width: "100%" }} />
            </div>
            <div className="col-lg-6">
              <p>
                A business visa serves as permission for individuals to travel abroad for business-related purposes. Typically issued to those engaging in activities like attending
                conferences, meetings, negotiations, or exploring business prospects in another country. These visas are generally valid for a specific duration, varying from a few
                days to several months based on the destination country and the purpose of the visit. Requirements and application processes for business visas differ across
                countries. However, applicants typically need documents such as an invitation letter from a business contact or organization in the destination country, proof of
                ample funds to support their stay, a valid passport, and sometimes additional supporting documents like a business plan or proof of business registration.
              </p>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default BusinessVisa;
