"use client";
import React from "react";
import studentImage from "../../assets/images/scholarship-student.png";
import Image from "next/image";

const StudentsStudySection = () => {
  return (
    <>
      <section className="flag-section">
        <div className="container">
          <div className="row">
            <div className="col-lg-6">
              <div className="flag-left">
                {" "}
                <Image className="student-img" src={studentImage} alt="student-img" />
              </div>
            </div>
            <div className="col-lg-6">
              <div className="flag-right">
                <div className="height-center">
                  <p className="flag-right-p1">20+ Best Universities Scholarship Programs From 20 Countries​</p>
                  <p className="flag-right-p2">We also help with other family based employment based and investment based Immigration.</p>
                  <ul className="ul-right-flag">
                    <li>{/* <Image className="flag-ul" src="https://www.ilcvisa.com/wp-content/uploads/2022/01/s-1-1.png" /> */}</li>
                    <li>{/* <Image className="flag-ul" src="https://www.ilcvisa.com/wp-content/uploads/2022/01/s-2-1.png" /> */}</li>
                    <li>{/* <Image className="flag-ul" src="https://www.ilcvisa.com/wp-content/uploads/2022/01/s-5.png" /> */}</li>
                    <li>{/* <Image className="flag-ul" src="https://www.ilcvisa.com/wp-content/uploads/2022/01/s-3.png" /> */}</li>
                    <li>{/* <Image className="flag-ul" src="https://www.ilcvisa.com/wp-content/uploads/2022/01/s-1-1.png" /> */}</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default StudentsStudySection;
