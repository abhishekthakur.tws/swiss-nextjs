"use client";
import React, { useState } from "react";
import canada from "../../assets/images/ca.svg";
import brazil from "../../assets/images/br.svg";
import arg from "../../assets/images/ar.svg";
import swiss from "../../assets/images/to.svg";
import ind from "../../assets/images/in.svg";
import newZealand from "../../assets/images/nz.svg";
import australiaFlag from "../../assets/images/au.svg";
import unitedKingdom from "../../assets/images/sh.svg";
import Image from "next/image";

const FlagSection = () => {
  const [flagData, setFlagData] = useState({
    title: "",
    description: "",
    title1: "",
    description1: "",
    title2: "",
    description2: "",
  });

  const handleFlagClick = (country) => {
    switch (country) {
      case "ind":
        setFlagData({
          title: "Visa Support",
          description:
            "Embark on your career journey amidst India's rich tapestry! Our consultancy specializes in securing work permits, turning your global ambitions into reality in this land of diverse opportunities. #WorkInIndia #VisaSupport",
          title1: "Visa Consultancy",
          description1:
            "Simplify your Indian work aspirations! Our tailored visa solutions streamline the process, ensuring a smooth path to obtaining your work permit. Let us guide you toward your Indian career dreams. #IndiaWorkPermit #VisaConsultancy",
          title2: "Work Permit",
          description2:
            "Realize your professional aspirations against India's vibrant backdrop! Our consultancy excels in securing work permits, eliminating visa application stress. Let's pave the way for your Indian career journey. #IndianWork #VisaSolutions",
        });
        break;
      case "canada":
        setFlagData({
          title: "Visa Support",
          description:
            "Chart your career course against Canada's diverse landscape! Our consultancy specializes in securing work permits, making your global ambitions a seamless reality in the land of opportunities. #WorkInCanada #VisaSupport",
          title1: "Visa Consultancy",
          description1:
            "Simplify your Canadian work aspirations! Our tailored visa solutions streamline the process, ensuring a hassle-free journey to obtaining your work permit. Let us pave the way for your Canadian career. #CanadaWorkPermit #VisaConsultancy",
          title2: "Work Permit",
          description2:
            "Turn your professional dreams into reality against Canada's picturesque backdrop! Our consultancy excels in securing work permits, eliminating visa hurdles. Let's pave the path for your Canadian career journey. #CanadianWork #VisaSolutions",
        });
        break;
      case "brazil":
        setFlagData({
          title: "Visa Support",
          description:
            "Brazil is blessed with a diverse geography, encompassing the Amazon rainforest, the world's largest tropical rainforest, and the Pantanal, the largest tropical wetland. It also boasts a lengthy coastline along the Atlantic Ocean, with famous beaches like Copacabana and Ipanema in Rio de Janeiro.",
          title1: "Visa Consultancy",
          description1:
            "Simplify your Brazilian work dreams! Our tailored visa solutions pave a hassle-free path, ensuring a smooth journey to obtaining your work permit. Let us guide you toward your professional aspirations. #BrazilWorkPermit #VisaConsultancy",
          title2: "Work Permit",
          description2:
            "Forge your career in the land of samba and opportunity! Our consultancy excels in securing work permits for Brazil, eliminating visa application stress. Let's turn your professional aspirations into reality. #BrazilianWork #VisaSolutions",
        });
        break;
      case "arg":
        setFlagData({
          title: "Visa Support",
          description:
            "Embrace Argentina's allure while building your career! Our consultancy specializes in securing work permits, making your global ambitions a seamless reality amidst Argentina's vibrant culture. #WorkInArgentina #VisaSupport",
          title1: "Visa Consultancy",
          description1:
            "Unlock opportunities in Argentina hassle-free! Our tailored visa solutions simplify the process, ensuring a stress-free work permit experience. Let us guide you toward your Argentine work dreams. #ArgentinaWorkPermit #VisaConsultancy",
          title2: "Work Permit",
          description2:
            "Transform your career against Argentina's rich tapestry! Our consultancy excels in obtaining work permits, eliminating visa hurdles. Let's pave the way for your professional journey in Argentina. #ArgentineWork #VisaSolutions",
        });
        break;
      case "swiss":
        setFlagData({
          title: "Visa Support",
          description:
            "Elevate your career against Switzerland's stunning backdrop! Our visa consultancy specializes in securing work permits, making your global ambitions a seamless reality. #WorkInSwitzerland #VisaSupport.",
          title1: "Visa Consultancy",
          description1:
            "Swiss work dreams made easy! Our tailored visa solutions simplify the maze, securing your work permit hassle-free. Let us pave the way for your international career. #SwissWorkPermit #VisaConsultancy",
          title2: "Work Permit",
          description2:
            "From Swiss Alps to professional peaks! Our consultancy excels in obtaining work permits, eliminating visa application stress. Let's turn your career aspirations into reality. #SwissWork #VisaSolutions",
        });
        break;

      default:
        setFlagData({
          title: "VisaSupport",
          description:
            "Your global career starts here! Our visa consultancy specializes in securing work permits, making your international aspirations a seamless reality. #WorkPermit #VisaSupport.",
          title1: "VisaConsultancy",
          description1:
            "Dreaming of working overseas? Our visa consultancy offers tailored solutions, simplifying the visa maze and securing your work permit hassle-free. #WorkAbroad #VisaConsultancy.",
          title2: "WorkPermit",
          description2:
            "Explore new horizons professionally! Our visa consultancy specializes in obtaining work permits, taking the stress out of visa applications. Let's make your career dreams a reality. #WorkPermit #VisaSolutions",
        });
        break;
    }
  };

  return (
    <>
      <section className="flag-ui-section">
        <div className="container">
          <div className="row">
            <div className="col-lg-12 flag-img-main">
              <div className="flag-img">
                <Image src={newZealand} alt="newZealand-flag" className="flag-img" onClick={() => handleFlagClick("ind")}></Image>
              </div>
              <div className="flag-img">
                <Image src={canada} alt="canada-flag" className="flag-img" onClick={() => handleFlagClick("canada")}></Image>
              </div>
              <div className="flag-img">
                <Image src={australiaFlag} alt="australia-flag" className="flag-img" onClick={() => handleFlagClick("brazil")}></Image>
              </div>
              <div className="flag-img">
                <Image src={unitedKingdom} alt="uk-flag" className="flag-img" onClick={() => handleFlagClick("arg")}></Image>
              </div>
              <div className="flag-img">
                <Image src={swiss} alt="swiss-flag" className="flag-img" onClick={() => handleFlagClick("swiss")}></Image>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="flag-section-data">
        <div className="container">
          <div className="row">
            <div className="col-lg-4">
              {/* <h2 className="flagdata-h">Explore Our Beautiful Destination</h2> */}
              <h2 className="flagdata-h">{flagData?.title}</h2>
              <p className="flagdata-p">{flagData?.description}</p>
              {/* <p className="flagdata-p">
                Welcome to our amazing tourist destination! Whether you're interested in historical landmarks, breathtaking landscapes, or vibrant cultural experiences, we have it
                all. Explore the wonders of our destination and create unforgettable memories.
              </p> */}
            </div>
            <div className="col-lg-4">
              {/* <h2 className="flagdata-h">Things to Do</h2> */}
              <h2 className="flagdata-h">{flagData?.title1}</h2>
              <ul>
                <li className="flagdata-p">{flagData?.description1}</li>
                {/* <li className="flagdata-p">{flagData?.description1}</li>
                <li className="flagdata-p">{flagData?.description1}</li>
                <li className="flagdata-p">{flagData?.description1}</li> */}
                {/* <li className="flagdata-p">Visit historical sites</li> */}
                {/* <li className="flagdata-p">Explore natural wonders</li> */}
                {/* <li className="flagdata-p">Indulge in local cuisine</li> */}
                {/* <li className="flagdata-p">Experience cultural events</li> */}
              </ul>
            </div>
            <div className="col-lg-4">
              {/* <h2 className="flagdata-h">Accommodation</h2> */}
              <h2 className="flagdata-h">{flagData?.title2}</h2>
              <p className="flagdata-p">{flagData?.description2}</p>
              {/* <p className="flagdata-p">
                Choose from a variety of accommodation options, ranging from luxury hotels to cozy bed and breakfasts. Whatever your preference, we have the perfect place for you
                to relax and recharge after a day of exploration.
              </p> */}
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default FlagSection;
