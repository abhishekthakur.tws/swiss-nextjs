"use client";
import React, { useState, useEffect } from "react";
import slide1 from "../../assets/images/slider-1.png";
import slide2 from "../../assets/images/slider-2.png";
import slide3 from "../../assets/images/slider-3.png";
import slide4 from "../../assets/images/slider-4.png";
import docs from "../../assets/images/docs.png";
import workVisa from "../../assets/images/work-visa.png";
import propertyMgt from "../../assets/images/proprty-mgts.png";
import studyAbroad from "../../assets/images/imgbin_study.png";
import "../user.css";
import Image from "next/image";
import Navbar from "./Navbar";
import { useRouter } from "next/navigation";
import FlagSection from "./FlagSection";
import CardsSection from "./CardsSection";
import StudentsStudySection from "./StudentsStudySection";
import InfoForm from "./InfoForm";

const Slider = () => {
  const router = useRouter();
  const [details, setDetails] = useState(1);
  const [currentSlide, setCurrentSlide] = useState(0);

  const handleSlideChange = (index) => {
    setCurrentSlide(index);
  };

  useEffect(() => {
    const interval = setInterval(() => {
      const newIndex = (currentSlide + 1) % slides.length;
      setCurrentSlide(newIndex);
    }, 3000);
    return () => clearInterval(interval);
  }, [currentSlide]);

  const slides = [
    {
      imageUrl: slide1,
      text: "Secure all necessary documents to embark on your journey abroad.",
      contentText: "Our motto is the seamless delivery of birth certificates, CENOMAR, passports, and other essential immigration documents.",
      contentImg: docs,
    },
    {
      imageUrl: slide2,
      text: "We specialize in obtaining work permits, fulfilling your needs effortlessly. ",
      contentText: "Swiis Consultant is your round-the-clock ally, dedicated to eliminating obstacles for the NRI community in their transition to settling abroad.",
      contentImg: workVisa,
    },
    {
      imageUrl: slide3,
      text: "Smooth the wrinkles in NRI property management ",
      contentText: "Discover the simplicity of purchasing, selling, and overseeing property in India with our seamless assistance! ",
      contentImg: propertyMgt,
    },
    {
      imageUrl: slide4,
      text: "Learning abroad is hassle-free!",
      contentText: "Turn your dream of studying at a prestigious international university into an effortlessly achievable reality with our seamless assistance.",
      contentImg: studyAbroad,
    },
  ];

  return (
    <>
      <section className="custom-slider">
        <div id="carouselExampleFade" className="carousel slide carousel-fade">
          <div className="carousel-inner">
            {slides.map((slide, index) => (
              <div className={`carousel-item ${index === currentSlide ? "active" : ""}`} key={index} onClick={() => handleSlideChange(index)}>
                <Image src={slide.imageUrl} className="d-block w-100" alt={`Slide ${index + 1}`} />
                <div className="text-overlay-abs">
                  <div className="slider-left-cus">
                    <h2>{slide.text}</h2>
                    <span>{slide.contentText}</span>
                  </div>
                  <div className="slider-right-cus">
                    <Image src={slide.contentImg} className="d-block w-100" alt={`Slide ${index + 1}`} />
                  </div>
                </div>
              </div>
            ))}
            <div className="carousel-indicators">
              {slides.map((_, index) => (
                <span key={index} className={`dot ${index === currentSlide ? "active" : ""}`} onClick={() => handleSlideChange(index)} />
              ))}
            </div>
          </div>
          <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleFade" data-bs-slide="prev">
            <span className="carousel-control-prev-icon" aria-hidden="true"></span>
            <span className="visually-hidden">Previous</span>
          </button>
          <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleFade" data-bs-slide="next">
            <span className="carousel-control-next-icon" aria-hidden="true"></span>
            <span className="visually-hidden">Next</span>
          </button>
        </div>
      </section>
      <CardsSection />
      <StudentsStudySection />
      <InfoForm />
      <FlagSection />

      {/* <section className="flag-section-data">
        <div className="container">
          <div className="row">
            <div className="col-lg-4">
              <h2 className="flagdata-h">Explore Our Beautiful Destination</h2>
              <p className="flagdata-p">
                Welcome to our amazing tourist destination! Whether you're interested in historical landmarks, breathtaking landscapes, or vibrant cultural experiences, we have it
                all. Explore the wonders of our destination and create unforgettable memories.
              </p>
            </div>
            <div className="col-lg-4">
              <h2 className="flagdata-h">Things to Do</h2>
              <ul>
                <li className="flagdata-p">Visit historical sites</li>
                <li className="flagdata-p">Explore natural wonders</li>
                <li className="flagdata-p">Indulge in local cuisine</li>
                <li className="flagdata-p">Experience cultural events</li>
              </ul>
            </div>
            <div className="col-lg-4">
              <h2 className="flagdata-h">Accommodation</h2>
              <p className="flagdata-p">
                Choose from a variety of accommodation options, ranging from luxury hotels to cozy bed and breakfasts. Whatever your preference, we have the perfect place for you
                to relax and recharge after a day of exploration.
              </p>
            </div>
          </div>
        </div>
      </section> */}
    </>
  );
};

export default Slider;
