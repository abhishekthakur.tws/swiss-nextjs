import { studentVisaInfo } from "@/app/redux/authActions";
import { Field, Form, Formik } from "formik";
import React, { useState } from "react";
import { useDispatch } from "react-redux";

const StudentVisaForm = () => {
  const [formdetail, setFormdetail] = useState({
    fullName: "",
    fatherName: "",
    dateOfBirth: "",
    gender: "",
    nationality: "",
    maritalStatus: "",
    address: "",
    email: "",
    phoneNumber: "",
    academicQualifications: "",
    Institution: "",
    Degree: "",
    sessionDate: "",
    currentCountry: "",
    destinationCountry: "",
    arrivalDate: "",
    passportNumber: "",
    englishProficiencyTestScores: "",
    bankStatements: "",
    healthInsuranceDetails: "",
    passportSizedPhotographs: "",
    scannedDocuments: "",
  });

  const [formErrors, setFormErrors] = useState({});

  const dispatch = useDispatch();

  // const validateForm = () => {
  //   const errors = {};

  //   if (!formdetail.fullName) {
  //     errors.fullName = "Required*";
  //   }

  //   if (!formdetail.fatherName) {
  //     errors.fatherName = "Required*";
  //   }

  //   if (!formdetail.dateOfBirth) {
  //     errors.dateOfBirth = "Required*";
  //   }

  //   if (!formdetail.gender) {
  //     errors.gender = "Required*";
  //   }

  //   if (!formdetail.nationality) {
  //     errors.nationality = "Required*";
  //   }

  //   if (!formdetail.maritalStatus) {
  //     errors.maritalStatus = "Required*";
  //   }

  //   if (!formdetail.address) {
  //     errors.address = "Required*";
  //   }

  //   if (!formdetail.email) {
  //     errors.email = "Required*";
  //   }

  //   if (!formdetail.phoneNumber) {
  //     errors.phoneNumber = "Required*";
  //   }
  //   if (!formdetail.academicQualifications) {
  //     errors.academicQualifications = "Required*";
  //   }

  //   setFormErrors(errors);
  //   return Object.keys(errors).length === 0;
  // };


  const validateForm = () => {
    const errors = {};
  
    if (!formdetail.fullName || !formdetail.fullName.trim()) {
      errors.fullName = "Required*";
    }
  
    if (!formdetail.fatherName || !formdetail.fatherName.trim()) {
      errors.fatherName = "Required*";
    }
    if (!formdetail.dateOfBirth|| !formdetail.dateOfBirth.trim()) {
      errors.dateOfBirth = "Required*";
    }

    if (!formdetail.gender|| !formdetail.gender.trim()) {
      errors.gender = "Required*";
    }

    if (!formdetail.nationality|| !formdetail.nationality.trim()) {
      errors.nationality = "Required*";
    }

    if (!formdetail.maritalStatus|| !formdetail.maritalStatus.trim()) {
      errors.maritalStatus = "Required*";
    }

    if (!formdetail.address|| !formdetail.address.trim()) {
      errors.address = "Required*";
    }

    if (!formdetail.email|| !formdetail.email.trim()) {
      errors.email = "Required*";
    }

    if (!formdetail.phoneNumber|| !formdetail.phoneNumber.trim()) {
      errors.phoneNumber = "Required*";
    }
       if (!formdetail.academicQualifications || !formdetail.academicQualifications.trim()) {
      errors.academicQualifications = "Required*";
    }
  
    setFormErrors(errors);
    return Object.keys(errors).length === 0;
  };

  const handleInputChange = (event) => {
    const { name, value, files, type } = event.target;

    if (type === "file") {
      setFormdetail((prevData) => ({
        ...prevData,
        [name]: files[0],
      }));
    } else {
      setFormdetail((prevData) => ({
        ...prevData,
        [name]: value,
      }));
    }
    validateForm();
  };

  const StudentonSubmit = (event) => {
    event.preventDefault();

    if (validateForm()) {
      dispatch(studentVisaInfo( formdetail ));
      setFormdetail({  fullName: "",
      fatherName: "",
      dateOfBirth: "",
      gender: "",
      nationality: "",
      maritalStatus: "",
      address: "",
      email: "",
      phoneNumber: "",
      academicQualifications: "",
      Institution: "",
      Degree: "",
      sessionDate: "",
      currentCountry: "",
      destinationCountry: "",
      arrivalDate: "",
      passportNumber: "",
      englishProficiencyTestScores: "",
      bankStatements: "",
      healthInsuranceDetails: "",
      passportSizedPhotographs: "",
      scannedDocuments: "",});
    }
  };
  return (
    <div>
      <div className="form">
        <section className="container formInfo">
          <div className="row">
            <div className="col-lg-12">
              <h1 className="info-text" style={{ fontFamily: "sans-serif" }}>
                Please enter the following details <br />
              </h1>
            </div>
            <div className="col-lg-12">
              <h2 style={{ color: "#294908", fontFamily: "sans-serif" }}>
                Student - Visa Application process
              </h2>
            </div>
            <Formik
              initialValues={{
                fullName: "",
                fatherName: "",
                dateOfBirth: "",
                gender: "",
                nationality: "",
                maritalStatus: "",
                address: "",
                email: "",
                phoneNumber: "",
                academicQualifications: "",
                Institution: "",
                Degree: "",
                sessionDate: "",
                currentCountry: "",
                destinationCountry: "",
                arrivalDate: "",
                passportNumber: "",
                englishProficiencyTestScores: "",
                bankStatements: "",
                healthInsuranceDetails: "",
                passportSizedPhotographs: "",
                scannedDocuments: "",
              }}
              // validationSchema={validationSchema}
              // onSubmit={StudentonSubmit}
            >
              {({ errors, touched, setFieldTouched }) => (
                <Form onSubmit={StudentonSubmit}>
                  <div className="container border border-dark mb-3 rounded submit-form">
                    <div className="row form_content">
                      <div className="col-lg-4">
                        <label htmlFor="fullName" className="form-label">
                          Full Name
                        </label>
                        <Field
                          type="text"
                          name="fullName"
                          id="fullName"
                          className="fields-width"
                          placeholder="Enter your fullName"
                          value={formdetail.fullName}
                          onChange={handleInputChange}
                        />

                        {formErrors.fullName && (
                          <span className="error" style={{ color: "red" }}>
                            {formErrors.fullName}
                          </span>
                        )}
                      </div>
                      <div className="col-lg-4">
                        <label htmlFor="fatherName" className="form-label">
                          Father Name
                        </label>
                        <Field
                          type="text"
                          name="fatherName"
                          id="fatherName"
                          className="fields-width"
                          placeholder="Enter your fatherName"
                          value={formdetail.fatherName}
                          onChange={handleInputChange}
                        />
                        {formErrors.fatherName && (
                          <span className="error" style={{ color: "red" }}>
                            {formErrors.fatherName}
                          </span>
                        )}
                      </div>
                      <div className="col-lg-4">
                        <label htmlFor="dateOfBirth" className="form-label">
                          Date Of Birth
                        </label>
                        <Field
                          type="date"
                          name="dateOfBirth"
                          id="dateOfBirth"
                          className="fields-width"
                          placeholder="Enter your Date Of Birth"
                          value={formdetail.dateOfBirth}
                          onChange={handleInputChange}
                        />
                        {formErrors.dateOfBirth && (
                          <span className="error" style={{ color: "red" }}>
                            {formErrors.dateOfBirth}
                          </span>
                        )}
                      </div>
                      <div className="col-lg-4">
                        <label htmlFor="gender" className="form-label">
                          Gender
                        </label>
                        <Field
                        as="select"
                        onChange={handleInputChange}
                        value={formdetail.gender}
                        name="gender"
                        id="gender"
                        className="fields-width gender"
                    >
                        <option value="" label="Select your gender" />
                        <option value="male" label="Male" />
                        <option value="female" label="Female" />
                        <option value="other" label="Other" />
                    </Field>
                        {formErrors.gender && (
                          <span className="error" style={{ color: "red" }}>
                            {formErrors.gender}
                          </span>
                        )}
                      </div>

                      <div className="col-lg-4">
                        <label htmlFor="nationality" className="form-label">
                          Nationality
                        </label>
                        <Field
                          type="text"
                          name="nationality"
                          id="nationality"
                          className="fields-width"
                          placeholder="Enter your Nationality"
                          value={formdetail.nationality}
                          onChange={handleInputChange}
                        />
                        {formErrors.nationality && (
                          <span className="error" style={{ color: "red" }}>
                            {formErrors.nationality}
                          </span>
                        )}
                      </div>
                      <div className="col-lg-4">
                        <label htmlFor="maritalStatus" className="form-label">
                          Marital Status
                        </label>
                        <Field
                        as="select"
                        name="maritalStatus"
                        id="maritalStatus"
                        className="fields-width gender"
                        value={formdetail.maritalStatus}
                          onChange={handleInputChange}
                    >
                        <option value="" label="Select your Status" />
                        <option value="Single" label="Single" />
                        <option value="Divorced" label="Divorced" />
                        <option value="Married" label="Married" />
                    </Field>
                        {formErrors.maritalStatus && (
                          <span className="error" style={{ color: "red" }}>
                            {formErrors.maritalStatus}
                          </span>
                        )}
                      </div>
                      <div className="col-lg-4">
                        <label htmlFor="address" className="form-label">
                          Address
                        </label>
                        <Field
                          type="text"
                          name="address"
                          id="phoneNumber"
                          className="fields-width"
                          placeholder="Enter your Address"
                          value={formdetail.address}
                          onChange={handleInputChange}
                        />
                        {formErrors.address && (
                          <span className="error" style={{ color: "red" }}>
                            {formErrors.address}
                          </span>
                        )}
                      </div>

                      <div className="col-lg-4">
                        <label htmlFor="email" className="form-label">
                          Email Address
                        </label>
                        <Field
                          type="email"
                          name="email"
                          id="email"
                          className="fields-width"
                          placeholder="Enter your Address"
                          value={formdetail.email}
                          onChange={handleInputChange}
                        />
                        {formErrors.email && (
                          <span className="error" style={{ color: "red" }}>
                            {formErrors.email}
                          </span>
                        )}
                      </div>
                      <div className="col-lg-4">
                        <label htmlFor="phoneNumber" className="form-label">
                          Phone Number
                        </label>
                        <Field
                          type="phoneNumber"
                          name="phoneNumber"
                          id="phoneNumber"
                          className="fields-width"
                          placeholder="Enter your Phone Number"
                          value={formdetail.phoneNumber}
                          onChange={handleInputChange}
                        />
                        {formErrors.phoneNumber && (
                          <span className="error" style={{ color: "red" }}>
                            {formErrors.phoneNumber}
                          </span>
                        )}
                      </div>
                      <div className="col-lg-4">
                        <label
                          htmlFor="academicQualifications"
                          className="form-label"
                        >
                          Academic Qualifications
                        </label>
                        <Field
                          type="text"
                          name="academicQualifications"
                          id="academicQualifications"
                          className="fields-width"
                          placeholder="Enter your Academic Qualifications "
                          value={formdetail.academicQualifications}
                          onChange={handleInputChange}
                        />
                        {formErrors.academicQualifications && (
                          <span className="error" style={{ color: "red" }}>
                            {formErrors.academicQualifications}
                          </span>
                        )}
                      </div>
                      <div className="col-lg-4">
                        <label htmlFor="Institution" className="form-label">
                          Institution
                        </label>
                        <Field
                          type="text"
                          name="Institution"
                          id="Institution"
                          className="fields-width"
                          placeholder="Enter your Institution Name "
                          value={formdetail.Institution}
                          onChange={handleInputChange}
                        />
                      </div>
                      <div className="col-lg-4">
                        <label htmlFor="Degree" className="form-label">
                          Degree
                        </label>
                        <Field
                          type="text"
                          name="Degree"
                          id="Degree"
                          className="fields-width"
                          placeholder="Enter your Degree "
                          value={formdetail.Degree}
                          onChange={handleInputChange}
                        />
                      </div>
                      <div className="col-lg-4">
                        <label htmlFor="sessionDate" className="form-label">
                          Session Date
                        </label>
                        <Field
                          type="date"
                          name="sessionDate"
                          id="sessionDate"
                          className="fields-width"
                          placeholder="Enter your Session"
                          value={formdetail.sessionDate}
                          onChange={handleInputChange}
                        />
                      </div>

                      <div className="col-lg-4">
                        <label htmlFor="currentCountry" className="form-label">
                          Current Country
                        </label>
                        <Field
                          type="text"
                          name="currentCountry"
                          id="currentCountry"
                          className="fields-width"
                          placeholder="Enter your Country"
                          value={formdetail.currentCountry}
                          onChange={handleInputChange}
                        />
                      </div>

                      <div className="col-lg-4">
                        <label
                          htmlFor="destinationCountry"
                          className="form-label"
                        >
                          Destination Country
                        </label>
                        <Field
                          type="text"
                          name="destinationCountry"
                          id="destinationCountry"
                          className="fields-width"
                          placeholder="Enter your Destination Country"
                          value={formdetail.destinationCountry}
                          onChange={handleInputChange}
                        />
                      </div>
                      <div className="col-lg-4">
                        <label htmlFor="arrivalDate" className="form-label">
                          Arrival Date
                        </label>
                        <Field
                          type="date"
                          name="arrivalDate"
                          id="arrivalDate"
                          className="fields-width"
                          placeholder="Enter your Arrival Date"
                          value={formdetail.arrivalDate}
                          onChange={handleInputChange}
                        />
                      </div>

                      <div className="col-lg-4">
                        <label htmlFor="passportNumber" className="form-label">
                          Passport Number
                        </label>
                        <Field
                          type="text"
                          name="passportNumber"
                          id="passportNumber"
                          className="fields-width"
                          placeholder="Enter your Passport Number"
                          value={formdetail.passportNumber}
                          onChange={handleInputChange}
                        />
                      </div>

                      <div className="col-lg-4">
                        <label
                          htmlFor="englishProficiencyTestScores"
                          className="form-label"
                        >
                          English Proficiency Test Scores
                        </label>
                        <Field
                          type="text"
                          name="englishProficiencyTestScores"
                          id="englishProficiencyTestScores"
                          className="fields-width"
                          placeholder="Enter your proficiency test scores  "
                          value={formdetail.englishProficiencyTestScores}
                          onChange={handleInputChange}
                        />
                      </div>

                      <div className="col-lg-4">
                        <label htmlFor="bankStatements" className="form-label">
                          Bank Statements
                        </label>
                        <Field
                          type="file"
                          name="bankStatements"
                          id="bankStatements"
                          className="fields-width"
                          placeholder="Enter your Bank Statement"
                          // value={formdetail.bankStatements}
                          onChange={handleInputChange}
                        />
                      </div>

                      <div className="col-lg-4">
                        <label
                          htmlFor="health_insurance_details"
                          className="form-label"
                        >
                          Health Insurance Details
                        </label>
                        <Field
                          type="file"
                          name="healthInsuranceDetails"
                          id="healthInsuranceDetails"
                          className="fields-width"
                          placeholder="Enter your Health Insurance Details"
                          // value={formdetail.healthInsuranceDetails}
                          onChange={handleInputChange}
                        />
                      </div>

                      <div className="col-lg-4">
                        <label
                          htmlFor="passportSizedPhotographs"
                          className="form-label"
                        >
                          Passport Sized Photographs
                        </label>
                        <Field
                          type="file"
                          name="passportSizedPhotographs"
                          id="passportSizedPhotographs"
                          className="fields-width"
                          placeholder="Enter your Passport Sized Photographs"
                          // value={formdetail.passportSizedPhotographs}
                          onChange={handleInputChange}
                        />
                      </div>
                      <div className="col-lg-4">
                        <label
                          htmlFor="scannedDocuments"
                          className="form-label"
                        >
                          Scanned Documents
                        </label>
                        <Field
                          type="file"
                          name="scannedDocuments"
                          id="scannedDocuments"
                          className="fields-width"
                          placeholder="Enter your scanned documents"
                          // value={formdetail.scannedDocuments}
                          onChange={handleInputChange}
                        />
                      </div>

                      <div className="col-lg-12 mb-2 d-flex align-item-end sbt-btn">
                        <button
                          className="info-submit-form btn btn-secondary "
                          type="submit"
                        >
                          Cancel
                        </button>
                        &nbsp;
                        <button className="info-submit-form" type="submit">
                          Submit
                        </button>
                      </div>
                    </div>
                  </div>
                </Form>
              )}
            </Formik>
          </div>
        </section>
      </div>
    </div>
  );
};

export default StudentVisaForm;
