"use client";
import { useDispatch } from "react-redux";
import React, { useEffect, useState } from "react";
import logoPic from "../../assets/images/logo.cdr.png";
import rabit from "../../assets/images/rabbit.png"
import Image from "next/image";
import { useRouter, usePathname } from "next/navigation";


const Navbar = () => {
  const router = useRouter();
  const pathname = usePathname()

  const [token, setToken] = useState("");
  const [role, setRole] = useState("");

  useEffect(() => {
    setToken(localStorage.getItem("accessToken"));
    setRole(localStorage.getItem("role"));
  }, [router]);

  const logoutFunction = () => {

    localStorage.clear();  

    if (role === "ADMIN") {
      router.push("/login")
    } else if (role === "USER") {
      router.push("/login")
    } else if (role === "EMPLOYEE") {
      router.push("/employelogin")
    } else if (role === "") {
      router.push("/login")
    }
  };

  
  const logoImage = pathname === '/user/pages/debtManagement' ? (
    <Image className="logo-img" alt="rabit-img" src={rabit} />
  ) : (
    <Image className="logo-img" alt="logo-img" src={logoPic} />
  );

  return (
    <>
      <section className="custom-header">
        <nav className="navbar navbar-expand-lg bg-body-tertiary">
          <div className="container nav-contain">
            <a className="navbar-brand" href="/">
              {logoImage}
            </a>
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li className="nav-item">
                <a href="/" className="nav-link" aria-current="page">
                  Home
                </a>
              </li>
              <li className="nav-item">
                <a href="/user/pages/about" className="nav-link">
                  About us
                </a>
              </li>
              <li className="nav-item">
                <a href="/user/pages/debtManagement" className="nav-link">
                  {" "}
                  Debt Management
                </a>
              </li>
              <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Our Services
                </a>
                <div className="dropdown-menu">
                  <a className="dropdown-item" href="/user/pages/studentVisa">
                    Student Visa
                  </a>
                  <a className="dropdown-item" href="/user/pages/touristAndVisitorVisa">
                    Tourist Visa
                  </a>
                  <a className="dropdown-item" href="/user/pages/workPermit">
                    Work Permit
                  </a>
                  <a className="dropdown-item" href="/user/pages/nriServices">
                    NRI Services
                  </a>
                </div>
              </li>
             {/**  <li className="nav-item">
                <a href="/user/pages/blog" className="nav-link">
                  {" "}
                  Blog
                </a>
              </li> **/}
              <li className="nav-item">
                <a href="/user/pages/contactUs" className="nav-link">
                  Contact us
                </a>
              </li>
            </ul>
            <ul className="right-button-nav">
            {token ? (
              <>
                <li>
                  <button onClick={logoutFunction}>Logout</button>
                </li>
                {role === "ADMIN" && window.location.href !== "http://43.205.48.160:3056/admin/pages/dashboard" && (
                  <li>
                    <button onClick={() => router.push("/admin/pages/dashboard")}>Admin Home</button>
                  </li>
                )}
              </>
            ) : (
              <>
                <li>
                  <button onClick={() => router.push("/login")} className="animateButton">Sign In</button>
                </li>
                <li>
                  <button onClick={() => router.push("/employelogin")}>Employee Login</button>
                </li>
              </>
            )}
          </ul>
          
          

          </div>
        </nav>
      </section>
    </>
  );
};

export default Navbar;
