"use client";
import React, { useState } from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import { useDispatch } from "react-redux";
import { userDetails } from "../../redux/authActions";

const InfoForm = () => {
  const dispatch = useDispatch();
  const [formErrors, setFormErrors] = useState({});

  const validateForm = () => {
    const errors = {};

    if (!userInfo.name) {
        errors.name = " *Required ";
    }

    if (!userInfo.email) {
        errors.email = " *Required ";
    }
    if (!userInfo.contactNo) {
      errors.contactNo = " *Required ";
  }

    setFormErrors(errors);
    return Object.keys(errors).length === 0;
};

  const [userInfo, setUserInfo] = useState({
    name: "",
    email: "",
    contactNo: "",
    remarks: "",
    Destination: "",
    referedBy: "",
  });

  const handleInputChange = (e) => {
    validateForm()
    setUserInfo({
      ...userInfo,
      [e.target.name]: e.target.value,
    });
    
  };

  const userFormSubmit = (e) => {
     validateForm()
    e.preventDefault();
   
    dispatch(userDetails(userInfo));
    setUserInfo({
      name: "",
      email: "",
      contactNo: "",
      remarks: "",
      Destination: "",
      referedBy: "",
    });
    
    // resetForm();
  };



  return (
    <>
      <section className="container formInfo">
        <div className="row">
          <div className="col-lg-6" style={{ marginTop: "100px" }}>
            <h1 className="info-text">
              Need more information? <br />
              Fill in the details to get a call-back.
            </h1>
          </div>
          <div className="col-lg-6">
            <h2 style={{ color: "#294908" }}>For any query please contact us</h2>
            <Formik
              initialValues={{
                name: "",
                email: "",
                contactNo: "",
                remarks: "",
                designation: "",
              }}
              validationSchema={validateForm}
              onSubmit={(values) => {
                console.log(values);
              }}
            >
              {({ errors, touched }) => (
                <Form onSubmit={userFormSubmit}>
                  <div className="container">
                    <div className="row">
                      <div className="col-lg-6">
                        <label htmlFor="name" className="form-label">
                          Name
                        </label>
                        <Field name="name" className="fields-width" placeholder="Enter your name" onChange={handleInputChange} value={userInfo.name} />
                        {formErrors.name && (
                          <span className="error" style={{ color: "red" }}>
                              {formErrors.name}
                          </span>
                      )}
                      </div>
                      <div className="col-lg-6">
                        <label htmlFor="contactNo" className="form-label">
                          Phone
                        </label>
                        <Field
                          type="number"
                          name="contactNo"
                          className="fields-width"
                          placeholder="Enter your contact number"
                          onChange={handleInputChange}
                          value={userInfo.contactNo}
                        />
                        {formErrors.contactNo && (
                          <span className="error" style={{ color: "red" }}>
                              {formErrors.contactNo}
                          </span>
                      )}                      </div>
                      <div className="col-lg-12">
                        <label htmlFor="email" className="form-label">
                          Email
                        </label>
                        <Field type="email" className="fields-width" name="email" placeholder="Enter your email" onChange={handleInputChange} value={userInfo.email} />
                        {formErrors.email && (
                          <span className="error" style={{ color: "red" }}>
                              {formErrors.email}
                          </span>
                      )}                      </div>
                      <div className="col-lg-12">
                        <label htmlFor="remarks" className="form-label">
                          Remarks
                        </label>
                        <Field
                          type="textarea"
                          name="remarks"
                          className="fields-width"
                          placeholder="Enter your remarks"
                          onChange={handleInputChange}
                          multiline
                          value={userInfo.remarks}
                        />
                      </div>
                      <div className="col-lg-6">
                        <div>
                          <label htmlFor="destination" className="form-label">
                            Destination
                          </label>
                          <Field
                            type="text"
                            name="Destination"
                            className="fields-width"
                            placeholder="Enter your destination"
                            onChange={handleInputChange}
                            value={userInfo.Destination}
                          />
                        </div>
                      </div>
                      <div className="col-lg-6">
                        <div>
                          <label htmlFor="referedBy" className="form-label">
                            Reffered by
                          </label>
                          <Field type="text" name="referedBy" className="fields-width" placeholder="Enter reffered by" onChange={handleInputChange} value={userInfo.referedBy} />
                        </div>
                      </div>

                      <div className="col-lg-12">
                        <button className="info-submit-form" type="submit">
                          Submit
                        </button>
                      </div>
                    </div>
                  </div>
                </Form>
              )}
            </Formik>
          </div>
        </div>
      </section>
    </>
  );
};

export default InfoForm;
