"use client";
import React from "react";
import Image from "next/image";
import card1 from "../../assets/images/Card1.jpg";
import card2 from "../../assets/images/card2.jpg";
import card3 from "../../assets/images/card3.jpg";
import card4 from "../../assets/images/working-employee.jpg";
import { useRouter } from "next/navigation";

const CardsSection = () => {
  const router = useRouter();

  return (
    <>
      <section className="visa-sec">
        <div className="container">
          <div className="row">
            <div className="col-lg-12" style={{ marginTop: "50px" }}>
              <div className="heading-top">
                <h1>
                  We Provide Visa <span>And Work permits</span>
                </h1>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-3" style={{ marginBottom: "50px" }}>
              <div className="common-visa">
                <div className="img-height">
                  <Image src={card1} className="img-sqr" alt="card-one"></Image>
                </div>
                <div className="img-height-1">
                  <h3 className="prop-p">TOURIST AND VISITOR VISA</h3>
                  <p className="prop-p-1">
                    Whether you’re a tourist looking to explore the wonders of our beautiful country or a visitor attending a special event or visiting family and friends.
                  </p>
                  <button className="prop-bt" onClick={() => router.push("/user/pages/touristAndVisitorVisa")}>
                    {" "}
                    Read more{" "}
                  </button>
                </div>
              </div>
            </div>
            <div className="col-lg-3" style={{ marginBottom: "50px" }}>
              <div className="common-visa">
                <div className="img-height">
                  <Image src={card2} className="img-sqr" alt="card-two"></Image>
                </div>
                <div className="img-height-1">
                  <h3 className="prop-p">STUDENT VISA</h3>
                  <p className="prop-p-1">
                    Studying abroad offers numerous benefits, including exposure to new cultures, access to high-quality education institutions, and the opportunity to broaden your
                    horizons.{" "}
                  </p>
                  <button className="prop-bt" onClick={() => router.push("/user/pages/studentVisa")}>
                    Read more
                  </button>
                </div>
              </div>
            </div>
            <div className="col-lg-3" style={{ marginBottom: "50px" }}>
              <div className="common-visa">
                <div className="img-height">
                  <Image src={card3} className="img-sqr" alt="card-three"></Image>
                </div>
                <div className="img-height-1">
                  <h3 className="prop-p">BUSINESS VISA</h3>
                  <p className="prop-p-1">
                    A business visa is a type of visa that allows individuals to travel to another country for business-related purposes. It is typically issued to individuals who
                    intend to engage in business activities.
                  </p>
                  <button className="prop-bt" onClick={() => router.push("/user/pages/businessVisa")}>
                    {" "}
                    Read more
                  </button>
                </div>
              </div>
            </div>
            <div className="col-lg-3" style={{ marginBottom: "50px" }}>
              <div className="common-visa">
                <div className="img-height">
                  <Image src={card4} className="img-sqr" alt="card-four"></Image>
                </div>
                <div className="img-height-1">
                  <h3 className="prop-p">WORK PERMIT</h3>
                  <p className="prop-p-1">
                    Our work permits enable individuals to effectively balance property and maintaining property. Our permits seamlessly integrate their work and essential
                    obligations.
                  </p>
                  <button className="prop-bt" onClick={() => router.push("/user/pages/nriServices")}>
                    {" "}
                    Read more
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default CardsSection;
