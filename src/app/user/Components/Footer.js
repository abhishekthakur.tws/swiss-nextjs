"use client";
import React from "react";
import logo from "../../assets/images/logo.cdr.png";
import fb from "../../assets/images/facebook.png";
import twitter from "../../assets/images/twitter.png";
import instagram from "../../assets/images/instagram.png";
import linkedin from "../../assets/images/linkedin.png";
import Image from "next/image";

const Footer = () => {
  return (
    <section className="flag-section">
      <div className="container">
        <div className="row">
          <div className="col-lg-3" style={{ marginTop: "20px", marginBottom: "20px" }}>
            <Image className="logo-img" alt="logo-img" src={logo} style={{ marginLeft: "100px" }} />
            <p
              style={{
                fontSize: "15px",
                color: "white",
                marginTop: "10px",
                marginLeft: "20px",
              }}
            >
              Swiis Consultants Private Ltd.
            </p>
            <div className="d-flex my-3 footer-social-icons">
              <a href="https://www.facebook.com/">
                <Image src={fb} alt="fb-icon" width={50} height={50} style={{ borderRadius: "50px" }} />
              </a>
              <a href="https://www.twitter.com/">
                <Image src={twitter} alt="twittor-icon" width={50} height={50} style={{ borderRadius: "50px" }} />
              </a>
              <a href="https://www.instagram.com/">
                <Image src={instagram} alt="instagram-icon" width={50} height={50} style={{ borderRadius: "50px" }} />
              </a>
              <a href="https://www.linkedin.com/">
                <Image src={linkedin} alt="linkedin-icon" width={50} height={50} style={{ borderRadius: "50px" }} />
              </a>
            </div>
          </div>
          <div className="col-lg-2" style={{ marginTop: "20px", marginBottom: "20px" }}>
            {" "}
            <p style={{ fontSize: "1.5rem", textAlign: "left" }} className="font-weight-bold text-white mb-3">
              About
            </p>
            <div style={{ margin: "10px", fontSize: "1rem", textAlign: "left" }}>
              <a href="#" style={{ textDecoration: "none", color: "white" }}>
                About us
              </a>
            </div>
            <div style={{ margin: "10px", fontSize: "1rem", textAlign: "left" }}>
              <a href="#" style={{ textDecoration: "none", color: "white" }}>
                help
              </a>
            </div>
          </div>
          <div className="col-lg-2" style={{ marginTop: "20px", marginBottom: "20px" }}>
            {" "}
            <p style={{ fontSize: "1.5rem", textAlign: "left" }} className="font-weight-bold text-white mb-3">
              Contact Us
            </p>
            <div style={{ margin: "10px", fontSize: "1rem", textAlign: "left" }}>
              <a href="#" style={{ textDecoration: "none", color: "white" }}>
                Swiis Consultants Private Ltd
                <br /> Street no. 4, Bank Colony, Mahilpur, Hoshiarpur, Punjab, India 146105
              </a>
            </div>
            <div
              style={{
                margin: "10px",
                fontSize: "1rem",
                textAlign: "left",
                color: "white",
              }}
            >
              7527008800
            </div>
          </div>
          <div className="col-lg-2" style={{ marginTop: "20px", marginBottom: "20px" }}>
            {" "}
            <p style={{ fontSize: "1.5rem", textAlign: "left" }} className="font-weight-bold text-white mb-3">
              Explore
            </p>
            <div style={{ margin: "10px", fontSize: "1rem", textAlign: "left" }}>
              <a href="#" style={{ textDecoration: "none", color: "white" }}>
                Property
              </a>
            </div>
            <div style={{ margin: "10px", fontSize: "1rem", textAlign: "left" }}>
              <a href="#" style={{ textDecoration: "none", color: "white" }}>
                General
              </a>
            </div>
            <div style={{ margin: "10px", fontSize: "1rem", textAlign: "left" }}>
              <a href="#" style={{ textDecoration: "none", color: "white" }}>
                Immigration
              </a>
            </div>
          </div>
          <div className="col-lg-3" style={{ marginTop: "20px", marginBottom: "20px" }}>
            <p style={{ fontSize: "1.5rem", textAlign: "left" }} className="font-weight-bold text-white mb-3">
              Know more
            </p>
            <div style={{ margin: "10px", fontSize: "1rem", textAlign: "left" }}>
              <a href="#" style={{ textDecoration: "none", color: "white" }}>
                Terms and conditions
              </a>
            </div>
            <div style={{ margin: "10px", fontSize: "1rem", textAlign: "left" }}>
              <a href="#" style={{ textDecoration: "none", color: "white" }}>
                Privacy policy
              </a>
            </div>
            <div style={{ margin: "10px", fontSize: "1rem", textAlign: "left" }}>
              <a href="#" style={{ textDecoration: "none", color: "white" }}>
                Contact us
              </a>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Footer;
