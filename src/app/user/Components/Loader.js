"use client";
import React from "react";
import "../../../app/globals.css";

const Loader = () => {
  return (
    <div className="loading-screen">
      <div className="loading-spinner"></div>
    </div>
  );
};

export default Loader;
