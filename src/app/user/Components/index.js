"use client";
import React, { useState, useEffect } from "react";
import Navbar from "./Navbar";
import Footer from "./Footer";
import Slider from "./Slider";

export default function AdminLayout(props) {
  const { children } = props;
  const [userData, setUserData] = useState(null);

  useEffect(() => {
    if (typeof window !== "undefined") {
      const storedUserData = localStorage.getItem("userId");
      setUserData(storedUserData || undefined);
    }
  }, []);

  return (
    <>
      <Navbar />
      <Slider />
      <Footer />
    </>
  );
}
