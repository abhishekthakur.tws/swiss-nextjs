"use client";
import React, { useState } from 'react'
import { useDispatch } from "react-redux";
import { Formik, Form, Field } from "formik";
import { touristVisaInfo } from '@/app/redux/authActions';
import { Input } from 'antd';


const TouristForm = () => {
    const dispatch = useDispatch();

    const [formErrors, setFormErrors] = useState({});

    const [userData, setUserdata] = useState({

        fullName: "",
        fatherName: "",
        dateOfBirth: "",
        gender: "",
        nationality: "",
        maritalStatus: "",
        address: "",
        email: "",
        phoneNumber: "",
        academicQualifications: "",
        Institution: "",
        Degree: "",
        sessionDate: "",
        currentCountry: "",
        destinationCountry: "",
        arrivalDate: "",
        passportNumber: "",
        passportIssueDate: "",
        passportExpiryDate: "",
        englishProficiencyTestScores: "",
        bankStatements: "",
        healthInsuranceDetails: "",
        passportSizedPhotographs: "",
        scannedDocuments: "",
        purposeOfVisit: "",
    })

    const validateForm = () => {
        const errors = {};

        if (!userData.fullName) {
            errors.fullName = " *Required ";
        }

        if (!userData.fatherName) {
            errors.fatherName = " *Required ";
        }

        if (!userData.dateOfBirth) {
            errors.dateOfBirth = " *Required ";
        }

        if (!userData.gender) {
            errors.gender = " *Required ";
        }

        if (!userData.nationality) {
            errors.nationality = " *Required ";
        }

        // if (!userData.bankStatements) {
        //     errors.bankStatements = " *Required ";
        // }

        if (!userData.address) {
            errors.address = " *Required ";
        }

        if (!userData.email) {
            errors.email = " *Required ";
        }

        if (!userData.phoneNumber) {
            errors.phoneNumber = " *Required ";
        }
        // if (!userData.scannedDocuments) {
        //     errors.scannedDocuments = " *Required ";
        // }
        // if (!userData.destinationCountry) {
        //     errors.destinationCountry = " *Required ";
        // }
        // if (!userData.passportSizedPhotographs) {
        //     errors.passportSizedPhotographs = " *Required ";
        // }

        setFormErrors(errors);
        return Object.keys(errors).length === 0;
    };

    const handleonchange = (event) => {
        const { name, value, files, type } = event.target;
        if (type === "file") {
            setUserdata((prevData) => ({
                ...prevData,
                [name]: files[0],
            }));
        } else {
            setUserdata((prevData) => ({
                ...prevData,
                [name]: value,
            }));
        }
        validateForm();
    };

    const FormonSubmit = (e) => {
        e.preventDefault();
        if (validateForm()) {
            dispatch(touristVisaInfo(userData));
            setUserdata({
                fullName: "",
                fatherName: "",
                dateOfBirth: "",
                gender: "",
                nationality: "",
                maritalStatus: "",
                address: "",
                email: "",
                phoneNumber: "",
                academicQualifications: "",
                Institution: "",
                Degree: "",
                sessionDate: "",
                currentCountry: "",
                destinationCountry: "",
                arrivalDate: "",
                passportNumber: "",
                passportIssueDate: "",
                passportExpiryDate: "",
                englishProficiencyTestScores: "",
                bankStatements: "",
                healthInsuranceDetails: "",
                passportSizedPhotographs: "",
                scannedDocuments: "",
                purposeOfVisit: "",
            })
        }
    };


    const resetForm = () => {
        setUserdata({
            fullName: "",
            fatherName: "",
            dateOfBirth: "",
            gender: "",
            nationality: "",
            maritalStatus: "",
            address: "",
            email: "",
            phoneNumber: "",
            academicQualifications: "",
            Institution: "",
            Degree: "",
            sessionDate: "",
            currentCountry: "",
            destinationCountry: "",
            arrivalDate: "",
            passportNumber: "",
            passportIssueDate: "",
            passportExpiryDate: "",
            englishProficiencyTestScores: "",
            bankStatements: "",
            healthInsuranceDetails: "",
            passportSizedPhotographs: "",
            scannedDocuments: "",
            purposeOfVisit: "",
        })
    }

    // const FormonSubmit = (e) => {
    //     e.preventDefault();

    //         dispatch(touristVisaInfo(userData));
    //         setUserdata({})

    //     console.log("useDAata", userData)
    // };

    return (
        <div className="form">
            <section className="container formInfo">
                <div className="row">
                    <div className="col-lg-12">
                        <h1 className="info-text" style={{ fontFamily: "sans-serif" }}>
                            Please enter the following details <br />
                        </h1>
                    </div>
                    <div className="col-lg-12">
                        <h2 style={{ color: "#294908", fontFamily: "sans-serif" }}>E- Visa Application process</h2>
                        <Formik
                            initialValues={{
                                fullName: "",
                                fatherName: "",
                                dateOfBirth: "",
                                gender: "",
                                nationality: "",
                                maritalStatus: "",
                                address: "",
                                email: "",
                                phoneNumber: "",
                                academicQualifications: "",
                                Institution: "",
                                Degree: "",
                                sessionDate: "",
                                currentCountry: "",
                                destinationCountry: "",
                                arrivalDate: "",
                                passportNumber: "",
                                passportIssueDate: "",
                                passportExpiryDate: "",
                                englishProficiencyTestScores: "",
                                bankStatements: "",
                                healthInsuranceDetails: "",
                                passportSizedPhotographs: "",
                                scannedDocuments: "",
                                purposeOfVisit: "",
                            }}
                            // validationSchema={SignupSchema}
                            onSubmit={FormonSubmit}
                        >
                            {({ errors, touched }) => (
                                <Form onSubmit={FormonSubmit}>
                                    <div className="container border border-dark mb-3 rounded submit-form">
                                        <div className="row form_content">
                                            <div className="col-lg-4">
                                                <label htmlFor="name" className="form-label">
                                                    Full Name
                                                </label>
                                                <Field type="text" onChange={handleonchange} value={userData.fullName} name="fullName" id="fullName" className="fields-width" placeholder="Enter your fullName" />
                                                {formErrors.fullName && (
                                                    <span className="error" style={{ color: "red" }}>
                                                        {formErrors.fullName}
                                                    </span>
                                                )}</div>
                                            <div className="col-lg-4">
                                                <label htmlFor="name" className="form-label">
                                                    Father Name
                                                </label>
                                                <Field type="text" name="fatherName" onChange={handleonchange} value={userData.fatherName} id="fatherName" className="fields-width" placeholder="Enter your fatherName" />
                                                {formErrors.fatherName && (
                                                    <span className="error" style={{ color: "red" }}>
                                                        {formErrors.fatherName}
                                                    </span>
                                                )} </div>
                                            <div className="col-lg-4">
                                                <label htmlFor="dateOfBirth" className="form-label">
                                                    Date Of Birth
                                                </label>
                                                <Field type="date" onChange={handleonchange} value={userData.dateOfBirth} name="dateOfBirth" id="dob" className="fields-width" placeholder="Enter your Date Of Birth" />
                                                {formErrors.dateofbirth && (
                                                    <span className="error" style={{ color: "red" }}>
                                                        {formErrors.dateofbirth}
                                                    </span>
                                                )} </div>

                                            <div className="col-lg-4 hender">
                                                <label htmlFor="gender" className="form-label">
                                                    Gender
                                                </label>
                                                <Field
                                                    as="select"
                                                    onChange={handleonchange}
                                                    value={userData.gender}
                                                    name="gender"
                                                    id="gender"
                                                    className="fields-width gender"
                                                >
                                                    <option value="" label="Select your gender" />
                                                    <option value="male" label="Male" />
                                                    <option value="female" label="Female" />
                                                    <option value="other" label="Other" />
                                                </Field>
                                                {formErrors.gender && (
                                                    <span className="error" style={{ color: "red" }}>
                                                        {formErrors.gender}
                                                    </span>
                                                )}
                                            </div>


                                            <div className="col-lg-4">
                                                <label htmlFor="nationality" className="form-label">
                                                    Nationality
                                                </label>
                                                <Field type="text" onChange={handleonchange} value={userData.nationality} name="nationality" id="nationality" className="fields-width" placeholder="Enter your Nationality" />
                                                {formErrors.nationality && (
                                                    <span className="error" style={{ color: "red" }}>
                                                        {formErrors.nationality}
                                                    </span>
                                                )} </div>
                                            <div className="col-lg-4">
                                                <label htmlFor="address" className="form-label">
                                                    Address
                                                </label>
                                                <Field type="text" onChange={handleonchange} value={userData.address} name="address" id="address" className="fields-width" placeholder="Enter your Nationality" />
                                                {formErrors.address && (
                                                    <span className="error" style={{ color: "red" }}>
                                                        {formErrors.address}
                                                    </span>
                                                )} </div>
                                            <div className="col-lg-4">
                                                <label htmlFor="martial_status" className="form-label">
                                                    Marital Status
                                                </label>
                                                <Field
                                                as="select"
                                                name="maritalStatus"
                                                id="maritalStatus"
                                                className="fields-width gender"
                                                value={userData.maritalStatus}
                                                  onChange={handleonchange}
                                            >
                                                <option value="" label="Select your Status" />
                                                <option value="Single" label="Single" />
                                                <option value="Divorced" label="Divorced" />
                                                <option value="Married" label="Married" />
                                            </Field>                                            </div>

                                            <div className="col-lg-4">
                                                <label htmlFor="email" className="form-label">
                                                    Email
                                                </label>
                                                <Field type="email" onChange={handleonchange} value={userData.email} name="email" id="email" className="fields-width" placeholder="Enter your Email" />
                                                {formErrors.email && (
                                                    <span className="error" style={{ color: "red" }}>
                                                        {formErrors.email}
                                                    </span>
                                                )} </div>

                                            <div className="col-lg-4">
                                                <label htmlFor="phoneNumber" className="form-label">
                                                    Phone
                                                </label>

                                                <Field type="number" onChange={handleonchange} value={userData.phoneNumber} name="phoneNumber" id="phoneNumber" className="fields-width" placeholder="Enter your phone Number" />
                                                {formErrors.phoneNumber && (
                                                    <span className="error" style={{ color: "red" }}>
                                                        {formErrors.phoneNumber}
                                                    </span>
                                                )} </div>

                                            <div className="col-lg-4">
                                                <label htmlFor="passportNumber" className="form-label">
                                                    Passport Number
                                                </label>
                                                <Field type="text" onChange={handleonchange} value={userData.passportNumber} name="passportNumber" id="passport" className="fields-width" placeholder="Enter your Passport Number" />
                                            </div>


                                            <div className="col-lg-4">
                                                <label htmlFor="destinationCountry" className="form-label">
                                                    Destination Country     </label>
                                                <Field type="text" onChange={handleonchange} value={userData.destinationCountry} name="destinationCountry" id="destinationCountry" className="fields-width" placeholder="Enter your Destination Country" />
                                                {formErrors.destinationCountry && (
                                                    <span className="error" style={{ color: "red" }}>
                                                        {formErrors.destinationCountry}
                                                    </span>
                                                )} </div>

                                            <div className="col-lg-4">
                                                <label htmlFor="planned_arrival_date" className="form-label">
                                                    Planned Arrival Date
                                                </label>
                                                <Field type="date" onChange={handleonchange} value={userData.arrivalDate} name="arrivalDate" id="arrivalDate" className="fields-width" placeholder="Enter your Planned Arrival Date " />
                                            </div>

                                            <div className="col-lg-4">
                                                <label htmlFor="bank_statement" className="form-label">
                                                    Bank Statements
                                                </label>
                                                <input type="file" onChange={handleonchange} name="bankStatements" id="bank_statement" className="fields-width" placeholder="Enter your Bank Statement" />
                                                {formErrors.bankStatements && (
                                                    <span className="error" style={{ color: "red" }}>
                                                        {formErrors.bankStatements}
                                                    </span>
                                                )} </div>


                                            <div className="col-lg-4">
                                                <label htmlFor="passport_size_photographs" className="form-label">
                                                    Passport Sized Photographs
                                                </label>
                                                <input
                                                    type="file"
                                                    onChange={handleonchange}
                                                    name="passportSizedPhotographs"
                                                    id="passport_size_photographs"
                                                    className="fields-width"
                                                    placeholder="Enter your Passport Sized Photograph"
                                                />
                                                {formErrors.passportSizedPhotographs && (
                                                    <span className="error" style={{ color: "red" }}>
                                                        {formErrors.passportSizedPhotographs}
                                                    </span>
                                                )} </div>

                                            <div className="col-lg-4">
                                                <label htmlFor="scanned_passport_copy" className="form-label">
                                                    Scanned Passport Copy
                                                </label>
                                                <Input type="file" onChange={handleonchange} name="scannedDocuments" id="scanned_passport_copy" className="fields-width" placeholder="Enter your Scanned Passport Copy " />
                                                {formErrors.scannedDocuments && (
                                                    <span className="error" style={{ color: "red" }}>
                                                        {formErrors.scannedDocuments}
                                                    </span>
                                                )} </div>

                                            <div className="col-lg-12 mb-2 d-flex align-item-end sbt-btn">
                                                <button className="info-submit-form btn btn-secondary " type="submit" onClick={resetForm}>
                                                    Cancel
                                                </button>
                                                &nbsp;
                                                <button className="info-submit-form" type="submit">
                                                    Submit
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </Form>
                            )}
                        </Formik>
                    </div>
                </div>
            </section>
        </div>
    )
}

export default TouristForm