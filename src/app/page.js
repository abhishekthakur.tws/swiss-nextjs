"use client";
import styles from "./page.module.css";
import Slider from "./user/Components/Slider";
import Home from "./user/Components/index";

export default function Adminlayout() {
  return (
    <main className={styles.main}>
      <Home />
    </main>
  );
}
