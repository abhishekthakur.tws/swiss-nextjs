"use client";
import React, { useState, useEffect } from "react";
import Navbar from "../../../user/Components/Navbar";
import Footer from "../../../user/Components/Footer";
import { Space, Table, Select } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { AllQueries } from "../../../redux/authActions";



const Page = () => {
  const { Option } = Select;

  const dispatch = useDispatch();
  const [selectedValue, setSelectedValue] = useState("defaultOption");

  const { GetAllQueriesList } = useSelector((state) => state.slice);

  const handleChange = (value) => {
    setSelectedValue(value);
  };

  useEffect(() => {
    return(()=>{
      dispatch(AllQueries());
    })
  }, [])

  const columns = [
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Phone no",
      dataIndex: "phone_no",
      key: "phone_no",
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
    },
    {
      title: "Action",
      dataIndex: "Action",
      key: "Action",
      render: (text, record) => (
        <Space size="middle">
          <Select                       
            value={GetAllQueriesList.status}
            style={{ width: 120 }}
            onChange={handleChange}
          >
            <Option value="defaultOption">Pending</Option>
            <Option value="option1">In Progress</Option>
            <Option value="option2">Completed</Option>
          </Select>
        </Space>
      ),
    },
  ];

  return (
    <div >
   <Navbar />
   <div className="container table-div">
      <Table dataSource={GetAllQueriesList} columns={columns} />
   </div>
   <Footer />
</div>

  );
};

export default Page;
